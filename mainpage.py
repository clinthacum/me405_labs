'''!@file               mainpage.py
    @brief              The home page for the portfolio.
    @details            A central place to quickly browse projects, and to serve as a landing page when
                        visiting the portfolio.

    @mainpage

    @section sec_intro  Introduction
                        This is the documentation site for Chris Linthacum and Caleb Savard's ME305 Lab Group 6.
                        On this site you will find all documentation for the lab projects completed for the class.

    @section lab_1      Lab 0x01: Getting Started with Hardware
                        The first lab objective is to become familiar with hardware I/O by implementing a program
                        that cycles through three different LED blink states on the Nucleo L476.
                        Please see led_blinks.py for details.
                        For source code, visit <a href="https://bitbucket.org/clinthacum/me405_labs/src/main/Lab0x01/led_blinks.py">the Bitbucket Repo</a>
                        <br>
                        Here is a demo of the project in action:<br>
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/EA6BsHlIKgc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly
                        
    @section lab_2      Lab 0x02: Incremental Encoders
                        This lab consists of creating a driver for a rotary encoder, then creating a program that uses that driver
                        to gather data from the encoder and respond to character commands from the serial connection. For source code, visit <a href="https://bitbucket.org/clinthacum/me405_labs/src/main/Lab0x02/">the Bitbucket Repo</a>
                        For details, see main.py
                        Here is a demo of the project:<br>
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/45U0xC6PFZo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly

    @section hw_2       Homework 0x02
                        This homework assignment required us to develop a set of Kinetic and Kinematic equations to describe the motion of a platform and ball system.
                        This assignment is in preparation for our term project - balancing a ball on top of a platform. To view more, visit @ref homework_2 "Homework 2".

    @section lab_3_s    Lab 0x03: DC Motor Control
                        This lab assignment required us to develop a motor and driver class to control multiple motors using the DRV8847 motor driver. We also
                        expanded the user interface from Lab 2 to include more functionality like testing and motor control. Check it out at @ref lab_3 "Lab 3".

    @section lab_4_s    Lab 0x04: Closed Loop Speed Control
                        This lab continued to develop the program created in the previous labs, extending our interface by adding a proportional controller to the system.
                        A step-function response sub-program has been implemented to test the step-response of motor to a target velocity. See more here - @ref lab_4 "Lab 4".

    @section lab_5_s    Lab 0x05: Closed Loop Panel Balancing & IMU Data
                        This lab had us create a driver for an IMU to obtain device position, orientation, and velocity. With this information
                        we used our closed loop controller class to balance our platform. The deliverable for this project was a demo, and all relevant
                        information regarding this lab has been incorporated into the term project, as the project simply built off of this lab as a foundation.

    @section termProj_s Term Project
                        For our term project, we attempted to balance a ball on the platform we previously balanced. Using a cascaded controller to adjust the panel angle
                        to position the ball, we were able to keep a ball balanced on a platform for up to 25 seconds at a time. With further tuning and adjustment
                        to the controller we could likely have balanced the ball indefinitely. For more information, visit @ref termProj "Term Project."<br>
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/NKaWso9O1-o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly

    @section src_code   Source Code
                        To review source code for the modules covered in this documentation, click the direct links in each section description. To access
                        the complete repository, <a href="https://bitbucket.org/clinthacum/me405_labs/src/main">click here.</a>

    @author             Chris Linthacum
    @author             Caleb Savard

    @copyright          Copyright 2021 Chris Linthacum and Caleb Savard

    @date               February 13, 2022
'''


