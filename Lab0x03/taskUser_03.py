'''!@file           taskUser_03.py
    @brief          Manages the states for the User Interface task.
    @details        Manages the current state and executes state-related logic for 
                    all User Interface functionality. Cycles through states for processing
                    input, zeroing encoder position, and printing output.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           February 2, 2022
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb import USB_VCP
import micropython
import shares_03

S0_INIT = micropython.const(0)
S1_CMD = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_PRINT = micropython.const(3)
S4_INPUT = micropython.const(4)
S5_TEST = micropython.const(5)


def printLegend():
    '''!
    @brief          Prints the available user key commands to the console
    @details        Prints a table of available user input commands to the console to
                    preview the available user key commands.
    '''
    print("|------------------------------------------------------------|")
    print("|                   Available User Commands                  |")
    print("|------------------------------------------------------------|")
    print("| z or Z | Zero the position of encoder 1                    |")
    print("|------------------------------------------------------------|")
    print("| p or P | Print out position of encoder 1                   |")
    print("|------------------------------------------------------------|")
    print("| d or D | Print out the delta for encoder 1                 |")
    print("|------------------------------------------------------------|")
    print("| v or V | Print out the velocity for encoder 1              |")
    print("|------------------------------------------------------------|")
    print("|   m    | Prompt the user to enter a duty cycle for motor 1 |")
    print("|------------------------------------------------------------|")
    print("|   M    | Prompt the user to enter a duty cycle for motor 2 |")
    print("|------------------------------------------------------------|")
    print("| c or C | Clear a fault condition triggered by the DRV8847  |")
    print("|------------------------------------------------------------|")
    print("| g or G | Collect encoder 1 data for 30 seconds and print   |")
    print("|        | to PuTTY as separate columns:                     |")
    print("|        |   1. Time values in seconds (the first value      |")
    print("|        |      should be 0 seconds).                        |")
    print("|        |   2. Position value in radians.                   |")
    print("|        |   3. Velocity values in radians per second.       |")
    print("|------------------------------------------------------------|")
    print("| t or T | Start the testing interface                       |")
    print("|------------------------------------------------------------|")
    print("| s or S | End data collection prematurely                   |")
    print("|------------------------------------------------------------|")
    print("\n")


def printTestLegend():
    '''!@brief          Prints the available user key commands in test interface to the console
        @details        Prints a table of available user input commands to the console to
                        preview the available user key commands in the testing interface.
    '''
    print("|------------------------------------------------------------|")
    print("|                   Available User Commands                  |")
    print("|------------------------------------------------------------|")
    print("| s or S | Exit the testing interface and return to main     |")
    print("|        | Also prints table of duty cycles and velocities   |")
    print("|------------------------------------------------------------|")
    print("\n")


def taskUserFcn(task_name: str, 
                period: int, 
                zFlag: shares_03.Share_03, 
                pFlag: shares_03.Share_03, 
                dFlag: shares_03.Share_03,
                vFlag: shares_03.Share_03,
                duty1: shares_03.Share_03,
                duty2: shares_03.Share_03,
                cFlag: shares_03.Share_03,
                gFlag: shares_03.Share_03,
                tFlag: shares_03.Share_03, 
                sFlag: shares_03.Share_03, 
                dataPrint: shares_03.Share_03,
                testData: shares_03.Share_03):
    '''!@brief              Main task function to control UI states.
        @details            Manage different User Input states, including Init, Read_Cmd, Zero_Encoder, and Print_Data.
                            On function run, executes the logic of the current state and if appropriate shifts 
                            state for next run of function.
        @param task_name    Task name for the function to help with debugging
        @param period       Period to run execute function at. Period defines frequency that states are executed and refreshed.
        @param zFlag        Shared data object to encapsulate the z key being pressed. Signals that encoder should be zeroed. 
        @param pFlag        Shared data object to encapsulate the p key being pressed. Signals to print the encoder position.
        @param dFlag        Shared data object to encapsulate the d key being pressed. Signals to print the encoder delta.
        @param gFlag        Shared data object to encapsulate the g key being pressed. Signals to begin data collection.
        @param sFlag        Shared data object to encapsulate the s key being pressed. Signals to end data collection prematurely.
        @param dataPrint    Shared data object to store the recorded encoder data. Stores both the position data and time values.
        @param testData     Shared data object to act as a flag to record avg velocity and to store the velocity data during testing.
        @yield state        If task ran, yields state the function is now in. If task did not evaluate, returns None.
    '''

    state = 0
    index_to_print = 0

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    serial_port = USB_VCP()

    return_state = S1_CMD
    s5_first_run = False
    s5_end_sig = False
    prompt_needed = True
    expecting_data = False

    new_duty = False

    test_mem = []

    buff = None

    printList = []

    while True:

        current_time = ticks_us()

        if (ticks_diff(current_time, next_time) >= 0):

            next_time = ticks_add(next_time, period)

            if state == S0_INIT:

                # Print out the introduction information and the legend here
                print("Welcome to the program.\n")
                printLegend()
                state = S1_CMD

            elif state == S1_CMD:

                # print(f"State = {state}")
                # Check to see if we have anything to print.
                # If we have long data, switch the state
                if dataPrint.read() is not None:
                    state = S3_PRINT

                # print("Checking Position")
                if pFlag.readData() is not None:
                    print("Print Pos:")
                    print(f"Encoder Position = {pFlag.readData()}")
                    pFlag.storeData(None)

                if dFlag.readData() is not None:
                    print(f"Encoder Delta = {dFlag.readData()}")
                    dFlag.storeData(None)

                if vFlag.readData() is not None:
                    print(f"Angular Velocity = {vFlag.readData()}")
                    vFlag.storeData(None)

                # Here we continously check for user input. We also process the user
                # input checking for values and setting the appropriate flags

                if serial_port.any():
                    read_char = serial_port.read(1).decode()

                    if read_char in ['z', 'Z']:
                        # Set flags to zero position of encoder
                        zFlag.write(True)
                        state = S2_ZERO

                    elif read_char in ['p', 'P']:
                        # Set flag to print out position of encoder
                        pFlag.write(True)

                    elif read_char in ['d', 'D']:
                        # Set flag to print out delta of encoder
                        dFlag.write(True)

                    elif read_char in ['v', 'V']:
                        # Set flag to print out velocity of encoder 1
                        vFlag.write(True)
                    
                    elif read_char == 'm':
                        # Switch state to input duty cycle
                        # Also set inputTar (m1Flag) True to know where to write (motor 1)
                        print("Enter a duty cycle for motor 1:")
                        inputTar = duty1
                        buff = ""
                        state = S4_INPUT
                        return_state = S1_CMD

                    elif read_char == 'M':
                        # Switch state to input duty cycle
                        # Also set inputTar (m2Flag) True to know where to write (motor 2)
                        print("Enter a duty cycle for motor 2:")
                        inputTar = duty2
                        buff = ""
                        state = S4_INPUT
                        return_state = S1_CMD

                    elif read_char in ['c', 'C']:
                        # Set flag to clear fault condition
                        cFlag.write(True)
                        print("Fault Cleared!")

                    elif read_char in ['g', 'G']:
                        # Collect encoder data
                        print("Data collection beginning...")
                        gFlag.write(True)

                    elif read_char in ['t', 'T']:
                        # Switch state to testing interface
                        state = S5_TEST
                        s5_first_run = True
                        printTestLegend()
                        tFlag.write(True)

                    elif read_char in ['s', 'S']:
                        # End data collection prematurely
                        if gFlag.read():
                            print("Stopping data collection...")
                            sFlag.write(True)

                    else:
                        print(f"Invalid character '{read_char}' entered\n")
                        # Clear the input buffer
                        serial_port.read()
                        printLegend()

            elif state == S2_ZERO:
                # Wait for zFlag to be set to false to return to state 1
                if not zFlag.read():
                    print("Done.")
                    state = S1_CMD

            elif state == S3_PRINT:
                # Iterate through printing until reached the end
                # For this state, print a list of lists, each list will be printed in its own column
                # print("State = 3")
                # # Old
                # time_data = dataPrint.readData()
                # data = dataPrint.read()
                if index_to_print == 0:
                    # This is the first run of this state
                    printData = dataPrint.readData()

                # print("PrintData = ", printData)
                if len(printData) == 2:
                    if index_to_print < len(printData[0]):
                        print(f"({printData[0][index_to_print]}, {printData[1][index_to_print]})")
                        index_to_print += 1
                    else:
                        index_to_print = 0
                        dataPrint.write(None)
                        dataPrint.storeData(None)
                        state = S1_CMD
                        # print("Done printing")
                if len(printData) == 3:
                    if index_to_print < len(printData[0]):
                        print(
                            f"({printData[0][index_to_print]}, {printData[1][index_to_print]}, {printData[2][index_to_print]})")
                        index_to_print += 1
                    else:
                        index_to_print = 0
                        dataPrint.write(None)
                        dataPrint.storeData(None)
                        state = S1_CMD
                        # print("Done printing")

                

            elif state == S4_INPUT:
                # This is where we read in multiple values to get a float value. buff is defined as
                # None when the state is changed to S4_INPUT. That way, whenever the input state is
                # called, buff is empty. I don't think there's an instance where the input state could
                # be called twice in a row without exiting the input state? --Caleb
                
                if serial_port.any():           # Check if there is a character, and read it as a string.
                    char = serial_port.read(1).decode()
                    
                    if char.isdigit():         # Check if it's a digit and add it.
                        buff += char
                    elif char == '-' and buff == "":    # Check if it's a minus at the beginning and add it.
                        buff += char
                    elif char in ['\b', '\0x08', '\x7F'] and not buff == "":  # Check if it's a backspace and NOT the first character (better method?)
                        buff = buff[:-1]
                    elif char in ['s', 'S'] and return_state == S5_TEST:
                        # Whenever S is pressed while in testing, need to quit immediately
                        s5_end_sig = True
                        state = S5_TEST

                    elif char in ['\r', '\n']:              # Check if it's enter, convert to float and assign
                        val = float(buff)                   # to target .share, then return to return_state
                        inputTar.write(val)
                        state = return_state
                        return_state = None
                        # inputTar.write(False)
                        new_duty = True
                        # Clear the buffer so its ready for next time
                        buff = ""
                        
                    print(buff)         # Print the buffer after each character input

            elif state == S5_TEST:
                # This opens up the testing interface
                # We already printed the legend before switching to this state
                if s5_first_run:
                    test_mem = []
                    s5_first_run = False
                    prompt_needed = True
                    expecting_data = False

                # Need to implement functionality for recording average velocity after the duty cycle is updated

                if s5_end_sig:
                    # print the data table
                    # return to S1_CMD
                    # Should use S3_PRINT to execute the print functionality

                    # Clear the test data for next time
                    testData.write(None)
                    testData.storeData(None)

                    try:
                        # We convert the list of tuples into two lists - one for time one for value
                        duty_out, vel_out = map(list, zip(*test_mem))

                        # Store the data sets to the dataPrint buffer
                        # In the S3, the print order is (readData(), read())
                        dataPrint.storeData([duty_out, vel_out])
                        dataPrint.write(True)
                    except ValueError:
                        # Because there were no values to unpack
                        pass

                    # Set state to 1 (state 1 automatically checks to see if there is anything to print and handles appropriately)
                    state = S1_CMD
                    s5_first_run = True
                    s5_end_sig = False
                    return_state = S1_CMD
                    print("Exited test interface")
                    yield

                if prompt_needed and not expecting_data:
                    # This is before we requested input through S4_INPUT
                    print("Choose a duty cycle for motor 1:")
                    prompt_needed = False

                    # Set inputTar to duty1 so that the float state knows where to write to
                    state = S4_INPUT
                    inputTar = duty1
                    return_state = S5_TEST
                    buff = ""

                if new_duty:
                    # This means we just returned from the input function
                    # Need to trigger a data point collection in testData
                    # We are using the testData.read() and testData.write() as the flag for this,
                    # with storeData and readData as the payload
                    # Once we've triggered data collection, we can restart and re-prompt for data
                    # print("Returned from input")
                    testData.write(True)
                    testData.storeData(None)
                    prompt_needed = True
                    expecting_data = True
                    new_duty = False

                if expecting_data and not testData.read():
                    # This is true when we are expecting data but testData.read() is false, meaning the 
                    # encoder is done reading data and has saved it to the payload
                    new_val = (duty1.read(), testData.readData())
                    testData.storeData(None)
                    test_mem.append(new_val)
                    expecting_data = False
                # else:
                    # print("Expecting new data point")

            else:
                raise ValueError(
                    f"Invalid state value in {task_name}: State {state} does not exist")

            yield state

        else:
            # Ticks diffnot yet ready to change state
            yield None
