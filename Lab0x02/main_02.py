'''!@file               main_02.py
    @brief              Main function to cycle through tasks and run them
    @details            Implements a simple loop to cycle through the tasks listed in task_list
                        one by one. 

                        For more information on how the program is structured and runs, review the diagrams below.
                        <br>
                        <img src="Lab2_state_diagram.png" alt="state diagrams for program 2" width="600">
                        
                        Below is a graph of the encoder output as collected by the g key.
                        <img src = "Resources/Lab2_graph.png" alt = "Encoder position graph">

    @author             Caleb Savard
    @author             Chris Linthacum
    @date               February 2, 2022
'''
import shares_02
import taskUser_02
import taskEncoder_02

def main():
    '''!@brief      Main driver executing taskUser.taskUserFcn() and taskEncoder.taskEncoderFcn()
        @details    The main driver function for Lab0x02. Cycles through 2 task management functions
                    (taskUser.taskUserFcn() and taskEncoder.taskEncoderFcn()) that manage states for the User
                    Interface and encoder management tasks. Terminates on KeyboardInterrupt.
        @returns    None
    '''
    zFlag = shares_02.Share_02(False)
    pFlag = shares_02.Share_02(False)
    dFlag = shares_02.Share_02(False)
    gFlag = shares_02.Share_02(False)
    sFlag = shares_02.Share_02(False)
    dataPrint = shares_02.Share_02(None)


    sample_period = 10000
    
    task_list = [taskUser_02.taskUserFcn_02("Task User", 10_000, zFlag, pFlag, dFlag, gFlag, sFlag, dataPrint), 
                 taskEncoder_02.taskEncoderFcn_02("Task Encoder", 10_000, sample_period, zFlag, pFlag, dFlag, gFlag, sFlag, dataPrint)]

    while True:
        try:
            for task in task_list:
                next(task)
        except KeyboardInterrupt:
            break
    
    print("Terminating Program")



if __name__ == '__main__':
    main()