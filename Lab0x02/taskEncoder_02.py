'''!@file           taskEncoder_02.py
    @brief          Manages the states for the encoder task.
    @details        Manages the current state and executes state-related logic for
                    all encoder related functionality. Cycles through state for initialization,
                    updating encoder, and zeroing encoder position.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           February 2, 2022
'''
# Import stuff
from time import ticks_us, ticks_add, ticks_diff
import encoder_02
import pyb
import shares_02
import micropython
import array

## Maps state 0 to a more readable const S0_INIT
S0_INIT = micropython.const(0)
## Maps state 1 to a more readable const S1_UPDATE
S1_UPDATE = micropython.const(1)
## Maps state 2 to a more readable const S2_ZERO
S2_ZERO = micropython.const(2)

# This task runs the Encoder Task state machine

def taskEncoderFcn_02(taskName: str, period: int, sample_period: int, zFlag: shares_02.Share_02, pFlag: shares_02.Share_02, dFlag: shares_02.Share_02, gFlag: shares_02.Share_02, sFlag: shares_02.Share_02, dataPrint: shares_02.Share_02):
    '''!@brief                  Function to execute the state management functionality for the encoder tasks.
        @details                Function manages states for the management of encoder functions. Performs all
                                encoder related tasks including position updates, checking and storing encoder
                                data, recording encoder position data, and zeroing the encoder when requested.
        @param taskName         Brief string to describe the instance of the function. Useful for debug purposes.
        @param period           Period with which to run the function and update/execute state logic.
        @param zFlag            Shared data object to encapsulate the z key being pressed. Signals that encoder should be zeroed. 
        @param pFlag            Shared data object to encapsulate the p key being pressed. Signals to print the encoder position.
        @param dFlag            Shared data object to encapsulate the d key being pressed. Signals to print the encoder delta.
        @param gFlag            Shared data object to encapsulate the g key being pressed. Signals to begin data collection.
        @param sFlag            Shared data object to encapsulate the s key being pressed. Signals to end data collection prematurely.
        @param sample_period      Period to record position data when recording position data vs. time.
        @param dataPrint        Shared data object to store the recorded encoder data. Stores both the position data and time values.
        @yield state            If task ran, yields state the function is now in. If task did not evaluate, returns None.
    
    '''
    # Initialize
    state = 0
    # print('We are in state 0. Initializing...')
    encoder1 = encoder_02.Encoder_02(1, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
    
    # Start timestamp system
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    # Create our data array for data collection
    data_array = None
    
    while True:
        current_time = ticks_us()
        
        if ticks_diff(current_time, next_time) >= 0:
            # print("Ran Task Encoder")
            next_time = ticks_add(next_time, period)
            
            if state == S0_INIT:
                # Initialize
                # print('State 0!')
                state = S1_UPDATE
            elif state == S1_UPDATE:
                # update the encoder
                # print("state 1")
                encoder1.update()
                
                # Activate any states from character inputs
                if zFlag.read() == True:
                    state = S2_ZERO
                    yield state
                    continue
                    
                    
                if pFlag.read() == True:
                    pFlag.storeData(encoder1.get_position())
                    
                    pFlag.write(False)
                    
                    
                if dFlag.read() == True:
                    dFlag.storeData(encoder1.get_delta())

                    dFlag.write(False)

                # Basic idea of how data record will work:
                # Check if gFlag == True
                # If true, then check to see if we just started data collection or not
                #   If data_array is None, we have not started data collection and need to start
                #       Need to initialize sampling counter/timer
                #       Need to initialize data_array
                #       Need to record first data point
                #   If data_array is not None, it is a list and we should collect next data point
                #       Check if time to collect next data point
                #           If yes, append data point to list
                # 
                # To end data collection prematurely:
                #   When sFlag is True, set the sample_end_time = current_time - 1
                #   This will cause data collection to stop immediately and the existing
                #   code will handle it gracefully as though we ended data collection normally
                
                if sFlag.read():
                    sample_end_time = current_time - 1

                    time_array = array.array('l', time_array[0:((current_time - sample_start_time) // sample_period)])
                    data_array = array.array('l', data_array[0:((current_time - sample_start_time) // sample_period)])
                    sFlag.write(False)
                    # print('Stopping data collection.')
                    # print(ticks_diff(sample_end_time, current_time))

                if gFlag.read():
                    # Check if we have already started data collection or not
                    if data_array is None:
                        # print('Starting data collection.')
                        # This means that we have not started collecting data yet
                        sample_start_time = current_time
                        sample_next_time = ticks_add(current_time, sample_period)

                        # Defined in the assignment as 30 seconds
                        data_collection_duration = 30 * 10**6    # in microseconds       
                        sample_end_time = ticks_add(sample_start_time, data_collection_duration)

                        # Make the array the size of collection duration / sample period rounded down
                        time_array = array.array('l', 1*[0])
                        data_array = array.array('l', 1*[0])
                        # Append a tuple of time collected and position value
                        data_array[0] = encoder1.get_position()
                        time_array[0] = current_time - sample_start_time
                    
                    else:
                        # Since we are already collecting data, check if time for next collection
                        if (ticks_diff(current_time, sample_end_time) > 0):
                            # print("In End IF Block:", current_time, sample_end_time)
                            # We are done collecting data as the 30 seconds has elapsed

                            # Print out the collected data by calling the handler function
                            # printDataCSV(data_array)
                            # Write the data to the printData share
                            dataPrint.write(data_array)
                            dataPrint.storeData(time_array)

                            # Since we have output the data, we now set data_array to None
                            # to prepare for the next time we want to collect
                            data_array = None
                            time_array = None

                            # Since we are done collecting data, we set gFlag to False
                            gFlag.write(False)

                        elif (ticks_diff(current_time, sample_next_time) >= 0):
                            # Find the index to write to by integer dividing diff between current 
                            # and start times by the sample period
                            sample_idx = (current_time - sample_start_time) // sample_period
                            data_array.extend(array.array('l', [encoder1.get_position()]))
                            time_array.extend(array.array('l', [current_time - sample_start_time]))
                            
                            # Set the next time to collect data
                            sample_next_time = ticks_add(sample_next_time, sample_period)

                    
            elif state == S2_ZERO:
                # Zero the encoder
                encoder1.zero()
                zFlag.write(False)
                state = S1_UPDATE
                
            else:
                # Error if there's a problem with the state machine
                raise ValueError('Invalid State!')
            
            yield state
            
        else:
            yield None

    