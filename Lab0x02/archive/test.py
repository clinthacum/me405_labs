import encoder
import time
import pyb

def main():

    encoder1 = encoder.Encoder(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)

    while True:
        try:
            print("Position = ", encoder1.get_position())
            encoder1.update()
            time.sleep(1)
        
        except KeyboardInterrupt:
            break

    print("Program Terminating")


if __name__ == "__main__":
    main()
