'''!@file                      led_blinks.py
    @brief                      On button activation, cycles through Square, Sinewave, and Sawtooth blink pattern
    @details                    When run, launches the main() function. Upon first booting/run, the program waits to receive
                                the first button press input. While waiting, the LED output is off. After the first button
                                press, the program will cycle through Square, Sine, and Sawtooth wave blink patterns. Each
                                time a new pattern is activated, it will restart the pattern from the beginning. To quit
                                the program while running, press the Keyboard Interrupt.

                                For more information on how the program is structured and runs, review the diagrams below.
                                <br>
                                <img src="Lab1_STD.png" alt="state diagrams for program" width="600">
    @author                     Chris Linthacum
    @author                     Caleb Savard
    @date                       January 20, 2021
    @license                    This package is licensed under the CC BY-NC-SA 4.0
                                Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                                for terms and conditions of the license.
'''

import pyb
import time
import math

def main():
    '''!
    @brief          Upon button activation, cycles through Square, Sine, and Sawtooth blink patterns
    @details        Upon first run, the program waits to receive the first button press input. After the first button
                    press, the LED output will cycle through a Square, Sine, and Sawtooth blink pattern on
                    each press of the input button.
    @param          None
    @return         None
    '''
    

    print("Welcome! Try cycling through the LED patterns on the Nucleo by pressing the blue B1 button on the board.")
    print("Available patterns include:\n     Square\n     Sinewave\n     Sawtooth\n")

    ## An integer to track and denote which LED blink state is currently active
    state = 0
    ## Global flag to track the press of the input button. Boolean True when flag is activated.
    global buttonPressed
    buttonPressed = False

    ## Pin object for the output LED
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

    ## Timer object to be used for PWM Control
    tim2 = pyb.Timer(2, freq=20000)

    ## Timer channel to control PWN output of PinA5
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

    ## Button press input pin
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)

    ## The ticks value returned from time.ticks_ms to be used as a 0 for the wave graphs
    start_time = None

    ## Configure pinC13 to interrupt on falling edge (button press)
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressCallback)

    while True:
        try:
            ##Code goes for FMS
            if state == 0:
                # no state /startup state. LED is not blinking yet.
                if buttonPressed:
                    buttonPressed = False
                    state = 1
                    print("Starting Square Wave")
                    start_time = time.ticks_ms()

            elif state == 1:
                #run Square wave

                ## Retrieve the brightness of the LED for the current time since start_time
                brt = squarePattern(time.ticks_diff(time.ticks_ms(), start_time))
                ## Set LED brightness
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    state = 2
                    print("Starting Sine Wave")
                    start_time = time.ticks_ms()
                
            elif state == 2:
                # run Sine Wave
                brt = sinePattern(time.ticks_diff(time.ticks_ms(), start_time))
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    state = 3
                    print("Starting Sawtooth Wave")
                    start_time = time.ticks_ms()

            elif state == 3:
                # run Sawtooth Pattern
                brt = sawtoothPattern(time.ticks_diff(
                    time.ticks_ms(), start_time))
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    state = 1
                    print("Starting Square Wave")
                    start_time = time.ticks_ms()


        except KeyboardInterrupt:
            break

    print("Program Terminating")

def squarePattern(t):
    '''!
    @brief          Outputs the value of a square wave pattern dependent on t
    @details        From a value t (ticks in ms) from the initial wave activation,
                    the function returns an output value equal to the amplitude of
                    a square wave at the given time t.
                    The square wave cycle has a period of 1 second, and an amplitude of 100.
    @param t        The number of ticks in miliseconds since the start (or zero point) of the pattern.
    @return brt     The amplitude of the wave pattern at the input time 't'
    '''
    remainder = t % 1000
    if remainder < 500:
        brt = 100
    else:
        brt = 0

    return brt

def sinePattern(t):
    '''!
    @brief          Outputs the value of a sine wave pattern dependent on t
    @details        From a value t (ticks in ms) from the initial wave activation,
                    the function returns an output value equal to the amplitude of
                    a sine wave at the given time t.
                    The sine wave cycle has a period of 10 seconds, and a peak-to-peak amplitude of 100 with an offset of +50.
    @param t        The number of ticks in miliseconds since the start (or zero point) of the pattern.
    @return brt     The amplitude of the wave pattern at the input time 't'
    '''
    remainder = t % 10000
    sec = remainder / 1000
    brt = 50 + 50 * math.sin(0.2 * math.pi * sec)
    return brt
    
def sawtoothPattern(t):
    '''!
    @brief          Outputs the value of a sawtooth wave pattern dependent on t
    @details        From a value t (ticks in ms) from the initial wave activation,
                    the function returns an output value equal to the amplitude of
                    a sawtooth wave at the given time t.
                    The square wave cycle has a period of 1 second, and an amplitude of 100.
                    The sawtooth pattern increases at a rate of 100 per second, and resets each
                    time it reaches the peak of 100.
    @param t        The number of ticks in miliseconds since the start (or zero point) of the pattern.
    @return brt     The amplitude of the wave pattern at the input time 't'
    '''
    remainder = t % 1000
    brt = remainder / 10
    return brt


def onButtonPressCallback(IRQ_src):
    '''!
    @brief          The callback function triggered by the physical button press. Sets buttonPressed flag to True.
    @details        When the physical input button is pressed on the device, this callback function sets
                    the global buttonPressed flag to True
    @param IRQ_src  The interrupt Source Register. Not used by this function, but contains the register 
                    responsible for the interrupt.
    '''
    global buttonPressed
    buttonPressed = True


if __name__ == "__main__":
    main()