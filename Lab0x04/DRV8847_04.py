'''!@file               DRV8847_04.py.py
    @brief              Driver for DRV8847
    @details            Implements driver class for DRV8847

    @author             Caleb Savard
    @author             Chris Linthacum
    @date               February 17, 2022
'''


import pyb
from motor_04 import Motor_04
from time import sleep, sleep_us

class DRV8847_04:
    '''!@brief      Driver class for the DRV8847 motor driver form Texas Instruments.
        @details    Used to initialize the driver, set up sleep and fault pins, but not timer channels.
    '''
    def __init__(self, sleep_pin: pyb.Pin, fault_pin: pyb.Pin, timer: int):
        '''!@brief      
            @details    Sets up sleep pin and fault pin, but not input pins (those get defined
                        in the motor class). Also creates timer object (at 20kHz) for use later.
        '''
        # print("Creating Driver Object...")
        
        # Configure timer
        self.timer = pyb.Timer(timer, freq=20000)
        # pins
        self.nSLEEP =  pyb.Pin(sleep_pin, mode=pyb.Pin.OUT_PP, pull=pyb.Pin.PULL_NONE)
        self.nFAULT =  pyb.Pin(fault_pin, mode=pyb.Pin.IN, pull=pyb.Pin.PULL_NONE)
        self.faultInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)

        
    def enable(self):
        '''!@brief
            @details
        '''
        # Set nSLEEP to high
        self.faultInt.disable()
        self.nSLEEP.high()
        sleep(50/1000000)
        self.faultInt.enable()
        
    def disable(self):
        '''!@brief
            @details
        '''
        # set nSLEEP to low
        self.nSLEEP.low()
        
    def fault_cb(self, IRQ_src):
        '''!@brief
            @details
        '''
        self.disable()
        print('Motor Fault! Disabled.')
        
    def motor(self, pin1, pin2, chan1, chan2):
        '''!@brief
            @details
        '''
        newMotor = Motor_04(self.timer, pin1, pin2, chan1, chan2)
        
        return newMotor
        
if __name__ == "__main__":
    # Create a new driver
    test_drv = DRV8847_04(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2, 3)

    # Create two motors
    motor1 = test_drv.motor(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2)
    motor2 = test_drv.motor(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4)

    test_drv.enable()

    test_ramp = False
    test_int = True

    if test_ramp:
        print("Motor 1: Duty Cycle = 40")
        motor1.set_duty(40)
        sleep(5)

        print("Motor 1: Duty Cycle = 70")
        motor1.set_duty(70)
        sleep(5)

        print("Motor 1: Duty Cycle = 100")
        motor1.set_duty(100)
        sleep(5)

        print("Motor 1: Duty Cycle = -50")
        motor1.set_duty(-50)
        sleep(5)

        print("Motor 1: Duty Cycle = 0")
        motor1.set_duty(0)

        print("Motor 2: Duty Cycle = 40")
        motor2.set_duty(40)
        sleep(5)

        print("Motor 2: Duty Cycle = 70")
        motor2.set_duty(70)
        sleep(5)

        print("Motor 2: Duty Cycle = 100")
        motor2.set_duty(100)
        sleep(5)

        print("Motor 2: Duty Cycle = -50")
        motor2.set_duty(-50)
        sleep(5)

        print("Motor 1: Duty Cycle = 0")
        motor2.set_duty(0)

    if test_int:
        
        motor1.set_duty(100)
        sleep(5)
        print("Flipping...")
        motor1.set_duty(-100)
        sleep(5)
        print("Toggling fault inturrupt")
        test_drv.enable()
        sleep(5)
        print('Running motor again')
        motor1.set_duty(-50)
        sleep(5)
        print('End')

    test_drv.disable()


