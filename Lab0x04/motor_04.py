'''!@file       motor_04.py
    @brief      Driver class for DC motor
    @details	For creating motor objects, initializing them, and calling relevant methods.
    @author	    Chris Linthacum
    @author     Caleb Savard
    @date       February 23, 2022
'''
import pyb
from pyb import Timer, Pin


class Motor_04:


    '''!@brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''

    def __init__ (self, PWM_tim: pyb.Timer, IN1_pin: pyb.Pin, IN2_pin: pyb.Pin, timChannel1: int, timChannel2: int):
        '''!@brief      Initializes and returns an object associated with a DC Motor
            @details    Objects of this class should not be instantiated 
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method 
                        DRV8847.motor()
                        Must specify correct timer channel to work with IN2_pin
            @param  PWM_tim     Passes in a timer object for generating the PWM signal.
            @param  IN1_pin     The first input pin for this motor. 
            @param  IN2_pin     The second input pin for this motor.
            @param  timChannel1 The first timer channel for the motor, attached to IN1_pin
            @param  timChannel2 The second timer channel for the motor, attached to IN2_pin
        '''

        # Configure our pins and timers
        self._IN1_pin = pyb.Pin(IN1_pin)
        self._IN2_pin = pyb.Pin(IN2_pin)

        self._PWM_tim = PWM_tim

        # Set up timer channel for PWM
        self._pin1ch = PWM_tim.channel(timChannel1, pyb.Timer.PWM, pin=self._IN1_pin)
        self._pin2ch = PWM_tim.channel(timChannel2, pyb.Timer.PWM, pin=self._IN2_pin)


    def set_duty (self, duty):
        '''!@brief      Set this PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values 
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                                cycle of the PWN signal sent to the motor
        '''
        if (duty > 100 or duty < -100):
            raise ValueError("duty must be less than 100 or greater than -100")

        if (duty >= 0):
            self._forward_motor(duty)
        else:
            self._reverse_motor(-duty)

    def _forward_motor(self, duty_cycle: int):

        self._pin1ch.pulse_width_percent(100)
        self._pin2ch.pulse_width_percent(100 - duty_cycle)

    def _reverse_motor(self, duty_cycle: int):

        self._pin1ch.pulse_width_percent(100 - duty_cycle)
        self._pin2ch.pulse_width_percent(100)



# if __name__ == '__main__':
#     # Adjust the following code to write a test program for your motor class. Any 
#     # # code within the if __name__ == '__main__' block will only run when the
#     # script is executed as a standalone program. If the script is imported as
#     # a module the code block will not run.

#     # Create a timer object to use for motor control
#     PWM_tim = pyb.Timer(3, freq=20_000)

#     # Create a motor driver object and two motor objects. You will need to
#     # modify the code to facilitate passing in the pins and timer objects needed # to run the motors.
#     motor_1 = Motor(PWM_tim, Pin.cpu.B4, Pin.cpu.B5, ...)

#     # Enable the motor driver
#     nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
#     nSLEEP.high()

#     # Set the duty cycle of the first motor to 40 percent
#     motor_1.set_duty(40)
