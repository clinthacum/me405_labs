'''!@file       taskMotor_04.py
    @brief      For running the motor and motor drivers.
    @details    Continuously updates the motors with desired duty cycles after initialization.
    @author     Caleb Savard
    @author     Chris Linthacum
    @date       February 23, 2022
'''
#Import stuff
import micropython
import shares_04
from DRV8847_04 import DRV8847_04
import pyb
from time import ticks_us, ticks_add, ticks_diff

## Maps state 0 to a more readable const S0_INIT
S0_INIT = micropython.const(0)
## Maps state 1 to a more readable const S1_COMMAND
S1_COMMAND = micropython.const(1)



def taskMotorFcn(taskName: str, 
                    period: int, 
                    duty1: shares_04.Share_04, 
                    duty2: shares_04.Share_04, 
                    cFlag: shares_04.Share_04):

    '''!@brief                  Function to execute the state management functionality for the motor tasks.
        @details                Function manages states for the management of motor functions. Performs all 
                                motor related processes including setting motor duty cycle, initialization, and clearing
                                faults. 
        @param taskName         Brief string to describe the instance of the function. Useful for debug purposes.
        @param period           Period with which to run the function and update/execute state logic.
        @param duty1            Shared data object to encapsulate the desired duty cycle of motor 1.
        @param duty2            Shared data object to encapsulate the desired duty cycle of motor 2.
        @param cFlag            Shared data object to signal that a fault needs to be cleared.
    '''
    
    state = 0
    
    # Start timestamp system
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
        
        # Check if its time to run
        if ticks_diff(current_time, next_time) >= 0:
            
            next_time = ticks_add(next_time, period)
            
            if state == S0_INIT:
                # Initialize the driver object and enable the driver.
                print('Initializing Motor Task')
                driver = DRV8847_04(sleep_pin = pyb.Pin.cpu.A15, fault_pin = pyb.Pin.cpu.B2, timer = 3)
                driver.enable()
            
                # Initialize motors
                motor1 = driver.motor(pin1 = pyb.Pin.cpu.B4, pin2 = pyb.Pin.cpu.B5, chan1 = 1, chan2 = 2)
                motor2 = driver.motor(pin1 = pyb.Pin.cpu.B0, pin2 = pyb.Pin.cpu.B1, chan1 = 3, chan2 = 4)

                # Set the initial duty cycles to 0
                duty1.write(0)
                duty2.write(0)
            
                # Send to the next state
                state = 1
            
                yield state
            
            if state == S1_COMMAND:
                #Set duty cycles.
                try:
                    motor1.set_duty(duty1.read())
                except ValueError:
                    print("ValueError: duty must be less than 100 or greater than -100. Saturation reached.")
                    if duty1.read() > 0:
                        duty1.write(100)
                    else:
                        duty1.write(-100)

                try:
                    motor2.set_duty(duty2.read())
                except ValueError:
                    print("ValueError: duty must be less than 100 or greater than -100. Saturation reached.")
                    if duty2.read() > 0:
                        duty2.write(100)
                    else:
                        duty2.write(-100)

                if cFlag.read():
                    driver.enable()
                    cFlag.write(False)
                
                yield state
                
# if __name__ == '__main__':
#     duty1 = shares_04.Share_04(50)
#     duty2 = shares_04.Share_04(25)
    
#     sample_period = 10000
    
#     task_list = [taskMotorFcn(taskName = 'Motor Task', period = 10_000, duty1 = duty1, duty2 = duty2)]

#     while True:
#         try:
#             for task in task_list:
#                 next(task)
#         except KeyboardInterrupt:
#             break
    
#     print("Terminating Program")
