'''!@file           taskController_04.py
    @brief          Sets up and calls the closed loop controller class.
    @details        Configures a proportional controller object and continuously updates the controller output value and assigns to the proper motor duty cycle.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           February 17, 2022
'''

import shares_04
from time import ticks_us, ticks_diff, ticks_add
import closedloop_04

def taskControllerFcn_04(taskName: str, 
                        period: int, 
                        input_signal: shares_04.Share_04, 
                        target_val: shares_04.Share_04, 
                        prop_gain: shares_04.Share_04, 
                        cont_enable: shares_04.Share_04, 
                        target_duty: shares_04.Share_04):
    '''!@brief      Sets up and calls the closed loop controller class.
        @details    Every period, this task resets the Kp using set_Kp and updates the controller. Saturation limits are set to 100 and -100.
        @param  taskName        Task name label for debugging purposes.
        @param  period          Period to run taskControllerFcn
        @param  input_signal    Shared variable to store controller input signal. In this case the actual encoder velocity.
        @param  target_val      Shared variable to store the target value for encoder velocity.
        @param  prop_gain       Shared variable to store the controller proportional gain value.
        @param  cont_enable     Shared variable to store a boolean whether to enable or disable the controller.
        @param  target_duty     Shared variable to point to the duty cycle to be affected by the controller.
    '''
    controller1 = closedloop_04.ClosedLoop_04(input_signal, target_val, prop_gain.read(), sat_limit_high = 100, sat_limit_low = -100)
    next_time = ticks_add(ticks_us(), period) 
    
    while True:
        # Initialize the timing system
        current_time = ticks_us()
        
        if cont_enable.read():
            
            # When it's time...
            if ticks_diff(current_time, next_time) >= 0:
            
                next_time = ticks_add(next_time, period)
                
                # Set the new Kp and update the controller.
                controller1.set_Kp(prop_gain.read())
                target_duty.write(controller1.update())
                # print(f"Kp = {controller1.Kp}, target_duty = {target_duty.read()}")
                
        yield None
                
                
            