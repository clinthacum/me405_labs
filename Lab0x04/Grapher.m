clear
close all

% Import all the data
str = fileread('step_response.txt');
% str = strrep(strrep(str, '(',''), ')','');
% str = strrep(str, ' ','');
% str = strrep(str, ',',' ');
stepData = sscanf(str,'%f',[3 Inf])';

str = fileread('K0_1.txt');
step0_1 = sscanf(str,'%f',[3 Inf])';

str = fileread('K0_25.txt');
step0_25 = sscanf(str,'%f',[3 Inf])';

str = fileread('K0_5.txt');
step0_5 = sscanf(str,'%f',[3 Inf])';

str = fileread('K0_75.txt');
step0_75 = sscanf(str,'%f',[3 Inf])';

str = fileread('K1_0.txt');
step1_0 = sscanf(str,'%f',[3 Inf])';

str = fileread('K1_15.txt');
step1_15 = sscanf(str,'%f',[3 Inf])';

target_vel = 50;
targ_times = [0, 0.99999999, 1, 3];
targ_vel_arr = [0, 0, target_vel, target_vel];


figure(1)
subplot(1, 2, 1);
%plot(stepData(:,1), stepData(:,2), 'Color', 'red')
hold on;
plot(targ_times, targ_vel_arr, 'Color', 'blue')
plot(step0_1(:,1), step0_1(:,2))
plot(step0_25(:,1), step0_25(:,2))
plot(step0_5(:,1), step0_5(:,2))
plot(step0_75(:,1), step0_75(:,2))
plot(step1_0(:,1), step1_0(:,2))
plot(step1_15(:,1), step1_15(:,2))
hold off;
title('Encoder Velocity vs. Time')
xlabel('Time [s]')
ylabel('Velocity [rads/sec]')
ylim([0, target_vel + 10])
legend('Target Speed','K = 0.1','K = 0.25', 'K = 0.5','K = 0.75','K = 1.0','K = 1.15')

subplot(1, 2, 2);
% Duty Cycle
%plot(stepData(:, 1), stepData(:, 3), 'Color', 'blue')
hold on
plot(step0_1(:,1), step0_1(:,3))
plot(step0_25(:,1), step0_25(:,3))
plot(step0_5(:,1), step0_5(:,3))
plot(step0_75(:,1), step0_75(:,3))
plot(step1_0(:,1), step1_0(:,3))
plot(step1_15(:,1), step1_15(:,3))
ylim([0, max(stepData(:, 3)) + 10])
title('% Duty Cycle vs. Time')
xlabel('Time [s]');
ylabel('% Duty Cycle');
legend('K = 0.1','K = 0.25', 'K = 0.5','K = 0.75','K = 1.0','K = 1.15')

