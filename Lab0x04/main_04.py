'''!@file               main_04.py
    @brief              Main function to cycle through tasks and run them
    @details            Implements a more advanced user interface and incorporates motor control.

    @author             Caleb Savard
    @author             Chris Linthacum
    @date               February 17, 2022
'''
import shares_04
import taskUser_04
import taskEncoder_04
import taskMotor_04
import taskController_04

def main():
    '''!@brief      Main driver executing taskUser.taskUserFcn() and taskEncoder.taskEncoderFcn()
        @details    The main driver function for Lab0x02. Cycles through 2 task management functions
                    (taskUser.taskUserFcn() and taskEncoder.taskEncoderFcn()) that manage states for the User
                    Interface and encoder management tasks. Terminates on KeyboardInterrupt.
        @returns    None
    '''
    zFlag = shares_04.Share_04(False)
    pFlag = shares_04.Share_04(False)
    dFlag = shares_04.Share_04(False)
    vFlag = shares_04.Share_04(False)
    gFlag = shares_04.Share_04(False)
    sFlag = shares_04.Share_04(False)
    duty1 = shares_04.Share_04(None)
    duty2 = shares_04.Share_04(None)
    dataPrint = shares_04.Share_04(None)
    cFlag = shares_04.Share_04(False)
    tFlag = shares_04.Share_04(False)
    testData = shares_04.Share_04(False)
    input_signal = shares_04.Share_04(None)
    target_val = shares_04.Share_04(0)
    prop_gain = shares_04.Share_04(0)
    cont_enable = shares_04.Share_04(None)
    # target_duty = shares_04.Share_04(None)
    
    sample_period = 100000
    
    task_list = [taskUser_04.taskUserFcn("Task User", 10_000, zFlag, pFlag, dFlag, vFlag, duty1, duty2, cFlag, gFlag, tFlag, sFlag, dataPrint, testData, target_val, prop_gain, cont_enable, input_signal), 
                 taskEncoder_04.taskEncoderFcn("Task Encoder", 10_000, sample_period, zFlag, pFlag, dFlag, gFlag, sFlag, dataPrint, testData, vFlag, input_signal),
                 taskMotor_04.taskMotorFcn("Task Motor", 10_000, duty1, duty2, cFlag),
                 taskController_04.taskControllerFcn("Task Controller", 10_000, input_signal, target_val, prop_gain, cont_enable, duty1)]

    while True:
        try:
            for task in task_list:
                next(task)
        except KeyboardInterrupt:
            break
    
    print("Terminating Program")



if __name__ == '__main__':
    main()