'''!@file       taskMotor_05.py
    @brief      For running the motors.
    @details    Continuously updates the motors with desired duty cycles after initialization.
    @author     Caleb Savard
    @author     Chris Linthacum
    @date       February 23, 2022
'''
#Import stuff
import micropython
import shares_05
import pyb
from motor_05 import Motor_05
from time import ticks_us, ticks_add, ticks_diff

## Maps state 0 to a more readable const S0_INIT
S0_INIT = micropython.const(0)
## Maps state 1 to a more readable const S1_COMMAND
S1_COMMAND = micropython.const(1)



def taskMotorFcn(taskName: str, 
                    period: int, 
                    roll_target_duty: shares_05.Share_05, 
                    pitch_target_duty: shares_05.Share_05):

    '''!@brief                  Function to execute the state management functionality for the motor tasks.
        @details                Function manages states for the management of motor functions. Performs all 
                                motor related processes including setting motor duty cycle, initialization, and clearing
                                faults. 
        @param taskName         Brief string to describe the instance of the function. Useful for debug purposes.
        @param period           Period with which to run the function and update/execute state logic.
        @param roll_target_duty            Shared data object to encapsulate the desired duty cycle of motor 1.
        @param pitch_target_duty            Shared data object to encapsulate the desired duty cycle of motor 2.
    '''
    
    state = 0
    
    # Start timestamp system
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
        
        # Check if its time to run
        if ticks_diff(current_time, next_time) >= 0:
            
            next_time = ticks_add(next_time, period)
            
            if state == S0_INIT:
                # # Initialize the driver object and enable the driver.
                # # print('Initializing Motor Task')
                # driver = DRV8847(sleep_pin = pyb.Pin.cpu.A15, fault_pin = pyb.Pin.cpu.B2, timer = 3)
                # driver.enable()

                # Initialize timer
                timer = pyb.Timer(3, freq=20000)

                # Define Pins
                motorXp1 = pyb.Pin.cpu.B0
                motorXp2 = pyb.Pin.cpu.B1
                motorXc1 = 3
                motorXc2 = 4

                motorYp1 = pyb.Pin.cpu.B4
                motorYp2 = pyb.Pin.cpu.B5
                motorYc1 = 1
                motorYc2 = 2
            
                # Initialize motors
                motorX = Motor_05(timer, motorXp1, motorXp2, motorXc1, motorXc2, name="motorX", invert_direction=True)
                motorY = Motor_05(timer, motorYp1, motorYp2, motorYc1, motorYc2, name="motorY", invert_direction=False)

                # Set the initial duty cycles to 0
                roll_target_duty.write(0)
                pitch_target_duty.write(0)
            
                # Send to the next state
                state = 1
            
                yield state
            
            if state == S1_COMMAND:
                #Set duty cycles.
                try:
                    target_duty = roll_target_duty.read()
                    motorX.set_duty(target_duty)
                    # print(f"X-duty = {target_duty}")
                    
                except ValueError:
                    print("ValueError: duty must be less than 100 or greater than -100. Saturation reached.")
                    if roll_target_duty.read() > 0:
                        roll_target_duty.write(100)
                    else:
                        roll_target_duty.write(-100)

                try:
                    target_duty = pitch_target_duty.read()
                    motorY.set_duty(target_duty)
                    # print(f"Y-duty = {target_duty}")
                    
                except ValueError:
                    print("ValueError: duty must be less than 100 or greater than -100. Saturation reached.")
                    if pitch_target_duty.read() > 0:
                        pitch_target_duty.write(100)
                    else:
                        pitch_target_duty.write(-100)

                # if cFlag.read():
                #     driver.enable()
                #     cFlag.write(False)
                
                yield state
                
# if __name__ == '__main__':
#     roll_target_duty = shares_05.Share_05(50)
#     pitch_target_duty = shares_05.Share_05(25)
    
#     sample_period = 10000
    
#     task_list = [taskMotorFcn(taskName = 'Motor Task', period = 10_000, roll_target_duty = roll_target_duty, pitch_target_duty = pitch_target_duty)]

#     while True:
#         try:
#             for task in task_list:
#                 next(task)
#         except KeyboardInterrupt:
#             break
    
#     print("Terminating Program")
