'''!@file               main_05.py
    @brief              Main function to cycle through tasks and run them
    @details            Implements a more advanced user interface and incorporates motor control.

    @author             Caleb Savard
    @author             Chris Linthacum
    @date               February 17, 2022
'''
import shares_05
import taskUser_05
import taskMotor_05
import taskController_05
import taskIMU_05
import time

from printqueue_05 import PrintQueue_05, PrintJob_05

def main_05():
    '''!@brief      Main driver executing taskUser.taskUserFcn() and taskEncoder.taskEncoderFcn()
        @details    The main driver function for Lab0x02. Cycles through 2 task management functions
                    (taskUser.taskUserFcn() and taskEncoder.taskEncoderFcn()) that manage states for the User
                    Interface and encoder management tasks. Terminates on KeyboardInterrupt.
        @returns    None
    '''
    zFlag = shares_05.Share_05(False)
    pFlag = shares_05.Share_05(False)
    dFlag = shares_05.Share_05(False)
    vFlag = shares_05.Share_05(False)
    gFlag = shares_05.Share_05(False)
    sFlag = shares_05.Share_05(False)
    roll_target_duty = shares_05.Share_05(None)
    pitch_target_duty = shares_05.Share_05(None)
    dataPrint = shares_05.Share_05(None)
    cFlag = shares_05.Share_05(False)
    tFlag = shares_05.Share_05(False)
    testData = shares_05.Share_05(False)
    input_signal = shares_05.Share_05(None)
    target_val = shares_05.Share_05(0)
    prop_gain = shares_05.Share_05(2)
    cont_enable = shares_05.Share_05(None)
    # target_duty = shares_05.Share_05(None)
    euler_angles = shares_05.Share_05(None)
    angular_velocities = shares_05.Share_05(None)
    calib_status = shares_05.Share_05(None)
    deriv_gain = shares_05.Share_05(0.1)
    roll_target_pos = shares_05.Share_05(0)
    pitch_target_pos = shares_05.Share_05(0)
    roll_target_deriv = shares_05.Share_05(0)
    pitch_target_deriv = shares_05.Share_05(0)

    print_queue = PrintQueue_05()
    
    sample_period = 100000

    time.sleep(1)
    
    task_list = [taskUser_05.taskUserFcn_05("Task User", 10_000, roll_target_duty, pitch_target_duty, gFlag, sFlag, prop_gain, deriv_gain, cont_enable, euler_angles, angular_velocities, roll_target_pos, pitch_target_pos, print_queue),
                 taskMotor_05.taskMotorFcn_05("Task Motor", 10_000, roll_target_duty, pitch_target_duty),
                 taskController_05.taskControllerFcn_05("Task Controller", 10_000, euler_angles, angular_velocities, roll_target_pos, roll_target_deriv, pitch_target_pos, pitch_target_deriv, prop_gain, deriv_gain, cont_enable, roll_target_duty, pitch_target_duty),
                 taskIMU_05.taskIMUFcn_05("Task IMU", 10_000, euler_angles, angular_velocities, calib_status, print_queue)]

    while True:
        try:
            for task in task_list:
                next(task)
        except KeyboardInterrupt:
            break
    
    print("Terminating Program")



if __name__ == '__main__':
    main_05()

    # Tested gains were Kp = 4.5 Kd = -0.2
