'''!@file               printqueue.py
    @brief              Implements a basic print queue to pass print statements to task User
    @details            Using a queue, pass print statements to the taskUser function so other tasks do not need to print.

    @author             Caleb Savard
    @author             Chris Linthacum
    @date               February 17, 2022
'''



from shares import Queue
from time import ticks_us, ticks_add, ticks_diff


class PrintJob():
    '''!@brief      A print job containing a string of text to print and an optional timeout parameter
        @details    A print job should be a short string to avoid taking too long to execute.
                    For longer print strings, break the string into multiple jobs and queue them
                    in order to have faster task execution.
    '''

    def __init__(self, data: str, timeout: int = 0):
        '''!@brief      Creates a new PrintJob object
                @details    Creates a new PrintJob object to hold a string of text to be printed by the PrintQueue
                @param  data            A short string that the job is containing
                @param  timeout         The number of microseconds before the print job should timeout (ie should no longer be printed)
        '''

        self.data = data
        self.time_scheduled = ticks_us()
        self.timeout = timeout


class PrintQueue():
    '''!@brief      A queue of print jobs to be printed by the user interface.
        @details    Values can be placed into queue and will be printed in the order
                    they have been queued. Will check a timeout for a print job if necessary
                    to avoid printing severely lagged data.
    '''

    def __init__(self):
        '''!@brief          Constructs an empty print queue
        '''

        self._queue = Queue()

    def add_job(self, new_job: PrintJob):
        '''!@brief      Adds a printjob to the queue
            @details    Must provide a printjob object to the function. Adds the job to the end of the queue.
            @param  new_job     PrintJob object to be added the print queue
        '''

        self._queue.put(new_job)

    def quick_print(self, text: str):
        '''!@ brief         Adds a printjob to the queue from a string
            @details        Must provide a string to the function. Adds the job to the end of the queue.
            @param  text    String of text to be printed
        '''
        self._queue.put(PrintJob(text))

    def get_job(self):
        '''!@brief      Get the next job from the print queue
            @details    Gets the next job from the print queue. Only returns a job if the timeout has not yet passed.
            @param  current_time    The current ticks value returned by ticks_us()
            @return     String to be printed
        '''
        current_time = ticks_us()
        foundJob = False
        while not foundJob:
            next_job = self._queue.get()

            if (next_job.timeout > 0) and (ticks_diff(current_time, next_job.time_scheduled) > next_job.timeout):
                # diff between current and time scheduled is greater than

                # # remove this line before production
                # print("Dropping Job...")
                foundJob = False
            else:
                # Timeout was not passed
                foundJob = True

        return next_job.data

    def num_in(self):
        '''!@brief      Find the number of items in the queue. Call before get().
            @return     The number of items in the queue
        '''
        return self._queue.num_in()
