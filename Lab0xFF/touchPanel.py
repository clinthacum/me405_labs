'''!@file           touchPanel.py
    @brief          Driver for a 4-wire resistive touch panel. 
    @details        This driver contains a method for scanning and calibrating the pad. Calibration corrects for scale, 
                    shift, rotation, and shear, outputing the corrected values in mm. The calibration relies on the user
                    touching points (-80, -40), (-80, 40), (0, 0), (80, -40), (80, 40), in that order during calibration. 
                    These points are processed against the ADC readings using the least squares method to develop a 
                    correlation. <br>
                    
                    A 4-wire resistive touch panel has two lengths of wire criss-crossing the surface. When pressure is applied, 
                    these two sets circuits contact each other. The scan method works by first setting a pin on each wire to 
                    high and low respectively. Current flows from one circuit to another, and the voltage at the connection point 
                    is proportional to the position along the touch panel. This voltage is read by an ADC connected to one of the 
                    other pins. It is important to note that the other pin must be 'floated', or disconnected to ensure that there 
                    is not voltage drop across the resistor leading to the ADC.<br>
                    
                    The scan method is somewhat optimized to run quickly. The first way this is done is by scanning in the X, Z, 
                    then Y, as pictured below. This minimizes the number of pin reassignments.<br>
                    
                    <img src="Resources/touchScanDiagram.png" alt="Scanning diagram" width="600"><br/> 
                    
                    To minimize error, the ADC 'oversamples' each reading using the ADC.read_timed() method, taking 5 readings 
                    and averaging them before moving on to the next scan. These readings are triggered by a timer are surprisingly 
                    faster than using the ADC.read() method for a single reading! Currently, the timer frequency is set to 80 KHz, 
                    and while it does appear to be able to read faster, we decided to keep it at this speed to prevent speed errors.<br>
                    
                    Another speed-increasing feature is the use of micropython.const() objects. These hold the pin assignments and can be 
                    accessed faster than the pin assignments themselves. All of these features mean that a single scan takes only 630 &mu;s.<br>
                    
                    <img src="Resources/touchScanSpeed.png" alt="Scanning speed" width="315"><br/>
                    
                    
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           March 18, 2022
'''


from printqueue import PrintQueue
import pyb
from pyb import Pin, ADC
import micropython
import time, array
from ulab import numpy as np
import shares

##  @brief      Pinmode IN
#   @details    A quicker way to reference the input (or float) pinmode
IN_CONST = micropython.const(pyb.Pin.IN)
##  @brief      Pinmode ANALOG
#   @details    A quicker way to reference the analog pinmode. I think this is actually unused.
ANALOG_CONST = micropython.const(pyb.Pin.ANALOG)
##  @brief      Pinmode OUT_PP
#   @details    A quicker way to reference the output push-pull pinmode
OUT_PP_CONST = micropython.const(pyb.Pin.OUT_PP)


class TouchPanel:
    '''!@brief      A driver class for a 4-wire resistive touch panel.
        @details    Contains methods for initializing, calibrating, and scanning a touch panel.
    '''
    
    def __init__(self, YmPin: pyb.Pin, YpPin: pyb.Pin, XmPin: pyb.Pin, XpPin: pyb.Pin, print_queue: PrintQueue = None):
        '''!@brief      Initializes a touchpad object.
            @details    Touchpad pin modes are preconfigured for the first X scan. A timer is also created for use 
                        by the ADC.read_timed() method. A buffer is also preallocated to hold the values from ADC.read_timed()
            @param      YmPin           Negative pin of the Y-dimension circuit.
            @param      YpPin           Positive pin of the Y-dimension circuit.
            @param      XmPin           Negative pin of the X-dimension circuit.
            @param      XpPin           Positive pin of the X_dimension circuit.
            @param      print_queue     A shared variable used for passing print statements out to the print function in taskUser.py
            
        '''
        ##  @brief      -Y Pin
        #   @details    Negative pin of the Y-dimension circuit.
        self.YmPin = YmPin
        ##  @brief      +Y Pin
        #   @details    Positive pin of the Y-dimension circuit.
        self.YpPin = YpPin
        ##  @brief      -X Pin
        #   @details    Negative pin of the X-dimension circuit.
        self.XmPin = XmPin
        ##  @brief      +X Pin
        #   @details    Positive pin of the X_dimension circuit.
        self.XpPin = XpPin
        
    
        self.YmPin.init(IN_CONST)
        self.YpPin.init(ANALOG_CONST)
        self.XmPin.init(OUT_PP_CONST, value = 0)
        self.XpPin.init(OUT_PP_CONST, value = 1)

        ##  @brief      Timer for the ADC
        #   @details    Used to trigger readings from ADC.read_timed()
        self.tim = pyb.Timer(6, freq=80000)
        ##  @brief      Buffer for readings
        #   @details    ADC readings are read into this buffer before being averaged. Changing it's size changes how many readings are made.
        self.buf = array.array("i", 5*[0])

        ##  @brief      Calibration Coefficients
        #   @details    An array holding the correlations used to convert ADC readings to useful positions.
        self.calibCoeffs = None
        
        ##  @brief      Print queue object
        #   @details    Items to be printed are added to the queue using this object.
        self.print_queue = print_queue
        

    
    def calibrate(self, touch_calib_status=None):
        '''!@brief      Calibrates the touchpad.
            @details    Requests 5 known points be pressed by the user: (-80, -40), (-80, 40), (0, 0), (80, -40), (80, 40)
                        These values are processed to determine calibration coefficients.
            @param      touch_calib_status  A shared variable used to indicate when the calibration process is finished.
            
        '''
        
        # self.print_queue.quick_print('CALLIBRATION MODE')
        self.print_queue.quick_print('Please touch 5 points on the touch pad')
        
        testPointsX = [0]*5
        testPointsY = [0]*5

        targetPoints = [[-80, -40], [-80, 40], [0, 0], [80, -40], [80, 40]]
        targetPointsArr = np.array(targetPoints)
        
        i = 0
        self.print_queue.quick_print(f"Please touch the grid point at ({targetPoints[i][0]} , {targetPoints[i][1]}) near the printed x, y, axis.")
        scanBuffer = []
        
        yield "Initialized calibration"
        
        while not testPointsX[-1]:
            
            currentVals = self.scan()
            if currentVals[2]:
                
                if (len(scanBuffer) < 10):
                    scanBuffer.append(currentVals)
                else:
                    xpts, ypts, zpts = zip(*scanBuffer)
                    testPointsX[i] = sum(xpts) / len(xpts)
                    testPointsY[i] = sum(ypts) / len(ypts)
                    
                    self.print_queue.quick_print(f'Point {i+1} registered: {testPointsX[i]}, {testPointsY[i]}')
                    scanBuffer = []
                    
                    i += 1
                    
                    yield
                    
                    # Wait for Z to be released to continue
                    while (self.scan()[2]):
                        pass
                    if not testPointsX[-1]:
                        self.print_queue.quick_print(f"Please touch the grid point at ({targetPoints[i][0]} , {targetPoints[i][1]})")
                        yield "Yield 1"
            
            yield f"Checking for data. currentVals={currentVals}; testPointsX={testPointsX}; testPointsY={testPointsY}"

                
                
        self.print_queue.quick_print('Points gathered.')
        # print(f'X = {testPointsX}')
        # print(f'Y = {testPointsY}')

        measuredVals = np.full((5, 3), 1.0)
        
        # Add the measured points to the measuredVals array
        for i in range(len(testPointsX)):
            measuredVals[i][0] = testPointsX[i]
            measuredVals[i][1] = testPointsY[i]

        self.print_queue.quick_print(measuredVals)

        calibCoeffs = np.dot(np.dot(np.linalg.inv(np.dot(measuredVals.T, measuredVals)), measuredVals.T), targetPointsArr)

        self.print_queue.quick_print("Calibration Coeffs:")
        self.print_queue.quick_print(calibCoeffs)

        self.calibCoeffs = calibCoeffs

        # print(calibCoeffs)

        with open("Panel_Cal_Coeffs.txt", 'w') as f:
            # CAL_CONST_0 = calibCoeffs[0][0]
            # CAL_CONST_1 = calibCoeffs[0][1]
            # CAL_CONST_2 = calibCoeffs[1][0]
            # CAL_CONST_3 = calibCoeffs[1][1]
            # CAL_CONST_4 = calibCoeffs[2][0]
            # CAL_CONST_5 = calibCoeffs[2][1]

            calibrationString = f"{calibCoeffs[0][0]}, {calibCoeffs[0][1]}, {calibCoeffs[1][0]}, {calibCoeffs[1][1]}, {calibCoeffs[2][0]}, {calibCoeffs[2][1]} "
            print(calibrationString)
            f.write(calibrationString)


        if touch_calib_status is not None:
            touch_calib_status.write(True)

        return calibCoeffs
  
    def scan(self):
        '''!@brief      Scans the touch panel and returns the position of contact.
            @details    The panel is scanned in X, Z, and Y, in that order. X and Y return the position in mm, and Z returns 
                        True if contact is made, otherwise, False is returned.
        '''
        
        
        # X-scan
        self.YmPin.init(IN_CONST)
        # self.YpPin.init(Pin.ANALOG)  I think we don't need this cause the ADC assignment takes care of it
        self.XmPin.init(OUT_PP_CONST, value = 0)
        self.XpPin.init(OUT_PP_CONST, value = 1)
        
        ##  @brief      ADC object for the X scans
        #   @details    Attached to the +Y pin
        self.xADC = ADC(self.YpPin)
        
        self.xADC.read_timed(self.buf, self.tim)
        xVal = sum(self.buf) / 5
        
        # Z-scan
        # YmPin doesn't change
        self.YpPin.init(OUT_PP_CONST, value = 1)
        # XmPin doesn't change
        # self.XpPin.init(Pin.ANALOG) I think we don't need this cause the ADC assignment takes care of it
        
        ##  @brief      ADC object for the Y and Z scans
        #   @details    Attached to the +X pin
        self.yzADC = ADC(self.XpPin)
        
        self.yzADC.read_timed(self.buf, self.tim)
        zVal = (sum(self.buf)>100)
        
        # Y-scan
        self.YmPin.init(OUT_PP_CONST, value = 0)
        # YpPin doesn't change
        self.XmPin.init(IN_CONST)
        # XpPin doesn't change
        
        self.yzADC.read_timed(self.buf, self.tim)
        yVal = sum(self.buf) / 5

        if self.calibCoeffs is not None:
            xVal = self.calibCoeffs[0][0] * xVal + self.calibCoeffs[1][0] * yVal + self.calibCoeffs[2][0]
            yVal = self.calibCoeffs[0][1] * xVal + self.calibCoeffs[1][1] * yVal + self.calibCoeffs[2][1]
        
        return (xVal, yVal, zVal)
    
if __name__ == '__main__':
    
    _i = 0
    _myPanel = TouchPanel(pyb.Pin.cpu.A0, pyb.Pin.cpu.A6, pyb.Pin.cpu.A1, pyb.Pin.cpu.A7)
    print('Starting test')
    _start_time = time.ticks_us()
    
    
    while (_i<1000):
        _val = _myPanel.scan()
        
        _i += 1
        
    _end_time = time.ticks_us()
    print(f'Time Elapsed per scan: {time.ticks_diff(_end_time, _start_time)/1000}')

# if __name__ == '__main__':
#     myPanel = TouchPanel(pyb.Pin.cpu.A0, pyb.Pin.cpu.A6, pyb.Pin.cpu.A1, pyb.Pin.cpu.A7)
    
#     while True:
#         try:
#             vals = myPanel.scan()
#             print(f'X = {vals[0]}, Y = {vals[1]}, Z = {vals[2]}')
#             time.sleep(0.01)
#         except KeyboardInterrupt:
#             break  
        
#     print('Terminating Program')


# if __name__ == '__main__':
#     myPanel = TouchPanel(pyb.Pin.cpu.A0, pyb.Pin.cpu.A6, pyb.Pin.cpu.A1, pyb.Pin.cpu.A7)

#     calibCoeffs = myPanel.calibrate()

#     while True:
#         try:
#             vals = list(myPanel.scan())
#             print(f'X = {vals[0]}, Y = {vals[1]}, Z = {vals[2]}')
#             time.sleep(0.01)
#         except KeyboardInterrupt:
#             break


#     print('Terminating Program')

            
