'''!@file           dcc.py
    @brief          File to wipe calibration constants from device filesystem
    @details        Running this script will attempt to delete calibration constant save files (if they exist).
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           March 16, 2022
'''

import os

if __name__ == "__main__":


    print(os.listdir())

    try:
        os.remove("IMU_cal_coeffs.txt")
        print("Successfully deleted imu calibration constant file.")

    except OSError:
        print("IMU Cal: File not exist error")

    try:
        os.remove("Panel_Cal_Coeffs.txt")
        print("Successfully deleted panel calibration constant file.")

    except OSError:
        print("Panel Cal: File not exist error")


