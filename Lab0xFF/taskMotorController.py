'''!@file           taskMotorController.py
    @brief          Sets up and calls the closed loop controller class for the inner loop.
    @details        Configures a PID controller object and continuously updates the controller output value and assigns to the proper motor duty cycle. 
                    In this case, the derivative of position for derivative control is drawn directly from the gyroscope data. This is more accurate 
                    than a numerical derivative.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           March 18, 2022
'''

import shares
from time import ticks_us, ticks_diff, ticks_add
import closedloop

def taskMotorControllerFcn(taskName: str, 
                        period: int, 
                        euler_angles: shares.Share,
                        angular_velocities: shares.Share,
                        roll_target_pos: shares.Share,
                        roll_target_deriv: shares.Share,
                        pitch_target_pos: shares.Share,
                        pitch_target_deriv: shares.Share,
                        prop_gain_roll: shares.Share, 
                        deriv_gain_roll: shares.Share,
                        int_gain_roll: shares.Share,
                        cont_enable: shares.Share, 
                        roll_target_duty: shares.Share,
                        pitch_target_duty: shares.Share,
                        prop_gain_pitch: shares.Share,
                        deriv_gain_pitch: shares.Share,
                        int_gain_pitch: shares.Share):
    '''!@brief                  Sets up and calls the closed loop controller class.
        @details                Every period, this task resets the gains using set_Kx() and updates the controller. 
                                Saturation limits are set to 100 and -100.
        @param  taskName        Task name label for debugging purposes.
        @param  period          Period to run taskControllerFcn
        @param  euler_angles    Shared variable to store the euler angles being recorded by the IMU.
        @param  angular_velocities  Shared variable to store angular velocities being recorded by the IMU.
        @param  roll_target_pos Shared variable to store the roll target position output from the angle controller.
        @param  roll_target_deriv   Shared variable to store the roll target velocity output from the angle controller (0).
        @param  pitch_target_pos    Shared variable to store the pitch target position output from the angle controller.
        @param  pitch_target_deriv  Shared variable to store the pitch target velocity output from the angle controller (0).
        @param  prop_gain_roll  Shared variable for the roll controller proportional gain
        @param  deriv_gain_roll Shared variable for the roll controller derivative gain.
        @param  int_gain_roll   Shared variable for the roll controller integral gain.
        @param  cont_enable     Shared variable for whether or not the controller is enabled.
        @param  roll_target_duty    Shared variable for the roll motor target duty cycle.
        @param  pitch_target_duty   Shared variable for the pitch motor target duty cycle.
        @param  prop_gain_pitch     Shared variable for the pitch controller proportional gain.
        @param  deriv_gain_pitch Shared variable for the pitch controller derivative gain.
        @param  int_gain_pitch   Shared variable for the pitch controller integral gain.       
    '''
    
    roll_stiction_comp = 1
    pitch_stiction_comp = 1

    try:
        roll_position_signal = shares.Share(euler_angles.read()['EUL_ROLL'])
        pitch_position_signal = shares.Share(euler_angles.read()['EUL_PITCH'])

        roll_deriv_signal = shares.Share(angular_velocities.read()['GYR_DATA_X'])
        pitch_deriv_signal = shares.Share(angular_velocities.read()['GYR_DATA_Y'])
    
    except TypeError:
        roll_position_signal = shares.Share(0)
        pitch_position_signal = shares.Share(0)

        roll_deriv_signal = shares.Share(0)
        pitch_deriv_signal = shares.Share(0)

    duty_sat_limit = 45
    int_sat_limit = 100
    
    roll_controller = closedloop.ClosedLoop(roll_position_signal, roll_deriv_signal, roll_target_pos, roll_target_deriv, prop_gain_roll.read(), deriv_gain_roll.read(), int_gain_roll.read(), sat_limit_high=duty_sat_limit, sat_limit_low=-duty_sat_limit, integral_limit=int_sat_limit,)
    pitch_controller = closedloop.ClosedLoop(pitch_position_signal, pitch_deriv_signal, pitch_target_pos, pitch_target_deriv, prop_gain_pitch.read(), deriv_gain_pitch.read(), int_gain_pitch.read(), sat_limit_high=duty_sat_limit, sat_limit_low=-duty_sat_limit, integral_limit=int_sat_limit)

    next_time = ticks_add(ticks_us(), period) 
    
    while True:
        # Initialize the timing system
        current_time = ticks_us()
        
        if cont_enable.read():
            
            # When it's time...
            if ticks_diff(current_time, next_time) >= 0:
                # print("Running controller update")
            
                next_time = ticks_add(next_time, period)

                # Update values for position and deriv signals
                # print(euler_angles.read())

                roll_position_signal.write(euler_angles.read()['EUL_ROLL'])
                pitch_position_signal.write(euler_angles.read()['EUL_PITCH'])

                roll_deriv_signal.write(angular_velocities.read()['GYR_DATA_X'])
                pitch_deriv_signal.write(angular_velocities.read()['GYR_DATA_Y'])
                
                # Update values for roll_controller
                roll_controller.set_Kp(prop_gain_roll.read())
                roll_controller.set_Kd(deriv_gain_roll.read())
                roll_controller.set_Ki(int_gain_roll.read())
                new_duty = roll_controller.update()
                roll_target_duty.write(new_duty * roll_stiction_comp)
                # print(f"Roll duty = {new_duty}")

                # Update values for pitch_controller
                pitch_controller.set_Kp(prop_gain_pitch.read())
                pitch_controller.set_Kd(deriv_gain_pitch.read())
                pitch_controller.set_Ki(int_gain_pitch.read())
                new_duty = pitch_controller.update()
                pitch_target_duty.write(new_duty * pitch_stiction_comp)
                # print(f"Pitch duty = {new_duty}")

                # print(f"Motor Y Duty = {pitch_target_duty.read()}")
                # print(f"Motor X Duty = {roll_target_duty.read()}")
                
        yield None
