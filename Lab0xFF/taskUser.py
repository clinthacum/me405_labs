'''!@file           taskUser.py
    @brief          Manages the states for the User Interface task.
    @details        Manages the current state and executes state-related logic for 
                    all User Interface functionality. States available are:<br>
                    <ol start="0">
                        <li>Initialization: Objects are instantiated and if sensors are not calibrated, triggers for the appropriate states 
                        are set. </li>
                        <li>Command: User input is recieved and appropriate triggers actions are taken.</li>
                        <li>IMU Calibration: The calibration method for the IMU is called.</li>
                        <li>Print: Methods from the PrintQueue are called to print any outstanding jobs.</li>
                        <li>Input: Multi-character input for things such as gain values are accepted in this state.</li>
                        <li>Panel Calibration: The calibration method for the touch panel is called.</li>
                        <li>Step Response: Perform a step response of the platform and log the results. These are later printed to the window.</li> 
                    </ol>
                    A state diagram is shown below.<br>
                    
                    <img src="Resources/FFtaskUserState.png" alt="taskUser State Diagram" width="600"><br/> 
                    
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           March 18, 2022
'''

from time import ticks_us, ticks_add, ticks_diff
import array

from printqueue import PrintJob, PrintQueue
from pyb import USB_VCP
import micropython
import shares
import math

##  @brief      State 0
#   @details    Maps state 0 to a more readable name
S0_INIT = micropython.const(0)
##  @brief      State 1
#   @details    Maps state 1 to a more readable name
S1_CMD = micropython.const(1)
##  @brief      State 2
#   @details    Maps state 2 to a more readable name
S2_IMUCAL = micropython.const(2)
##  @brief      State 3
#   @details    Maps state 3 to a more readable name
S3_PRINT = micropython.const(3)
##  @brief      State 4
#   @details    Maps state 4 to a more readable name
S4_INPUT = micropython.const(4)
##  @brief      State 5
#   @details    Maps state 5 to a more readable name
S5_PANELCAL = micropython.const(5)
##  @brief      State 6
#   @details    Maps state 6 to a more readable name
S6_SRESP = micropython.const(6)


def printLegend(print_queue: PrintQueue):
    '''!
    @brief          Prints the available user key commands to the console
    @details        Prints a table of available user input commands to the console to
                    preview the available user key commands.
    @param  print_queue A print queue object to add the legend print statements to
    '''
    print_queue.quick_print("|------------------------------------------------------------|")
    print_queue.quick_print("|                   Available User Commands                  |")
    print_queue.quick_print("|                 For more commands press 'd'                |")
    print_queue.quick_print("|------------------------------------------------------------|")
    print_queue.quick_print("| p or P | Print Euler angles (in degrees) from IMU          |")
    print_queue.quick_print("| v or V | Print out the angular velocities from IMU         |")
    print_queue.quick_print("| t or T | Print out set target for ball position and angle  |")
    print_queue.quick_print("| s or S | End data collection prematurely                   |")
    print_queue.quick_print("| a      | Enter target roll angle (when ball not present)   |")
    print_queue.quick_print("| A      | Enter target pitch angle (when ball not present)  |")
    print_queue.quick_print("| w or W | Enable or disable closed-loop control             |")
    print_queue.quick_print("| r or R | Record data to a file                             |")
    print_queue.quick_print("| x or X | Set both motor duty cycles to 0                   |") 
    print_queue.quick_print("|------------------------------------------------------------|")
    print_queue.quick_print("\n")

def printDevTools(print_queue: PrintQueue):
    '''!@brief          Prints the available dev tool key commands to the console
        @details        Prints a table of available user input commands oriented towards dev tools
                        to the console to preview the available user key commands.
        @param  print_queue A print queue object to add the legend print statements to
    '''
    print_queue.quick_print("|------------------------------------------------------------|")
    print_queue.quick_print("|                    Dev Tool User Commands                  |")
    print_queue.quick_print("|------------------------------------------------------------|")
    print_queue.quick_print("| l or L | Print out the last known touchPanel location      |")
    print_queue.quick_print("| k      | Enter new proportional gain (Kp) (roll motor)     |")
    print_queue.quick_print("| j      | Enter new derivative gain (Kd) (roll motor)       |")
    print_queue.quick_print("| h      | Enter new integral gain (Ki) (roll motor)         |")
    print_queue.quick_print("| K      | Enter new proportional gain (Kp) (pitch motor)    |")
    print_queue.quick_print("| J      | Enter new derivative gain (Kd) (pitch motor)      |")
    print_queue.quick_print("| H      | Enter new integral gain (Ki) (pitch motor)        |")
    print_queue.quick_print("| 1      | Enter new proportional gain (Kp) (angle x)        |")
    print_queue.quick_print("| 2      | Enter new derivative gain (Kd) (angle x)          |")
    print_queue.quick_print("| 3      | Enter new integral gain (Ki) (angle x)            |")
    print_queue.quick_print("| 4      | Enter new proportional gain (Kp) (angle y)        |")
    print_queue.quick_print("| 5      | Enter new derivative gain (Kd) (angle y)          |")
    print_queue.quick_print("| 6      | Enter new integral gain (Ki) (angle y)            |")
    print_queue.quick_print("| c or C | Print all control constants                       |")
    print_queue.quick_print("| i or I | Print angle controller signals and target         |")
    print_queue.quick_print("|------------------------------------------------------------|")
    print_queue.quick_print("\n")       


def taskUserFcn(task_name: str,
                period: int,
                roll_target_duty: shares.Share,
                pitch_target_duty: shares.Share,
                gFlag: shares.Share,
                sFlag: shares.Share,
                prop_gain_roll: shares.Share,
                deriv_gain_roll: shares.Share,
                cont_enable: shares.Share,
                euler_angles: shares.Share,
                angular_velocities: shares.Share,
                roll_target_pos: shares.Share,
                pitch_target_pos: shares.Share,
                x_target_position: shares.Share,
                y_target_position: shares.Share,
                print_queue: PrintQueue,
                ball_detected: shares.Share,
                x_position_signal: shares.Share,
                y_position_signal: shares.Share,
                imu_calib_status: shares.Share,
                touch_calib_status: shares.Share,
                positionx_prop_gain: shares.Share,
                int_gain_roll: shares.Share,
                positionx_deriv_gain: shares.Share,
                positionx_int_gain: shares.Share,
                positiony_prop_gain: shares.Share,
                positiony_deriv_gain: shares.Share,
                positiony_int_gain: shares.Share,
                controller_signalsx: shares.Share,
                controller_signalsy: shares.Share,
                prop_gain_pitch: shares.Share,
                deriv_gain_pitch: shares.Share,
                int_gain_pitch: shares.Share,
                data_collect_flag: shares.Share):
    '''!@brief              Main task function to control UI states.
        @details            Manage different User Input states, including Init, Read_Cmd, Zero_Encoder, and Print_Data.
                            On function run, executes the logic of the current state and if appropriate shifts 
                            state for next run of function.
        @param task_name    Task name for the function to help with debugging
        @param period       Period to run execute function at. Period defines frequency that states are executed and refreshed.
        @param roll_target_duty        Shared data object to store the duty cycle for motor 1.
        @param pitch_target_duty        Shared data object to store the duty cycle for motor 2.
        @param gFlag        Shared data object to encapsulate the g key being pressed. Signals to begin data collection.
        @param sFlag        Shared data object to encapsulate the s key being pressed. Signals to end data collection prematurely.
        @param  prop_gain_roll      Shared data object for the roll controller proportional gain.
        @param  deriv_gain_roll     Shared data object for the roll controller derivative gain.
        @param cont_enable  Shared data object to specificy whether closed-loop controller is enabled (True) or not
        @param  euler_angles        Shared data object for the euler angles recorded by the IMU.
        @param  angular_velocities  Shared data object for the angular velocities being recorded by the IMU.
        @param  roll_target_pos     Shared data object for the roll target position output from the angle controller.
        @param  pitch_target_pos    Shared data object for the pitch target position being output from the angle controller.
        @param  x_target_position   Shared data object for the target x position being fed to the angle controller.
        @param  y_target_position   Shared data object for the target y position being fed to the angle controller.
        @param  print_queue         Print queue object for passing out print statements.
        @param  ball_detected       Shared data object for if the ball is on the platform.
        @param  x_position_signal   Shared data object for the current X position of the ball.
        @param  y_position_signal   Shared data object for the current Y position of the ball.
        @param  imu_calib_status    Shared data object for the IMU Calibration status.
        @param  touch_calib_status  Shared data object for whether or not the touch panel is calibrated.
        @param  positionx_prop_gain Shared data object for the x position controller proportional gain.
        @param  int_gain_roll       Shared data object for the roll angle controller integral gain.
        @param  positionx_deriv_gain    Shared data object for the x position controller derivative gain.
        @param  positionx_int_gain      Shared data object for the x position controller integral gain.
        @param  positiony_prop_gain Shared data object for the y position controller proportional gain.
        @param  positiony_deriv_gain    Shared data object for the y position controller derivative gain.
        @param  positiony_int_gain      Shared data object for the y position controller integral gain. 
        @param  controller_signalsx      Output signal from the x position controller.
        @param  controller_signalsy      Output signal from the y position controller.
        @param  prop_gain_pitch         Pitch angle controller proportional gain.
        @param  deriv_gain_pitch        Pitch angle controller derivative gain.
        @param  int_gain_pitch          Pitch angle controller integral gain.
        @param  data_collect_flag       Shared variable to hold the data collection flag
        
    '''

    state = 0
    index_to_print = 0

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    serial_port = USB_VCP()

    return_state = S1_CMD
    s5_first_run = False
    s5_end_sig = False
    prompt_needed = True
    expecting_data = False

    new_duty = False

    buff = None

    printList = []

    setGain = False
    setDGain = False
    step_start_time = None
    temp_duty_cycle = None
    step_sample_period = 10_000
    step_sample_next = 0
    end_step_premature = False

    test_roll_data = None
    test_pitch_data = None
    sample_number = 0
    print("Welcome to the program.\n")

    while True:

        current_time = ticks_us()

        if (ticks_diff(current_time, next_time) >= 0):

            next_time = ticks_add(next_time, period)

            if state == S0_INIT:

                # Print out the introduction information and the legend here
                
                if (not imu_calib_status.read()):
                    state = S2_IMUCAL
                    print_queue.quick_print("IMU Calibration...")
                    imu_calib_status.storeData(False)
                elif(not touch_calib_status.read()):
                    state = S5_PANELCAL
                    print_queue.quick_print("Touch Panel Calibration...")
                    imu_calib_status.storeData(True)

                else:
                    printLegend(print_queue)
                    state = S1_CMD

            elif state == S1_CMD:

                # Here we continously check for user input. We also process the user
                # input checking for values and setting the appropriate flags

                if print_queue.num_in() > 0:
                    state = S3_PRINT

                # Check for user input
                if serial_port.any():
                    read_char = serial_port.read(1).decode()

                    if read_char in ['p', 'P']:
                        # Set print out Euler angles
                        print("Print IMU Position (in degrees):")
                        angles = euler_angles.read()
                        print(f" heading = {angles['EUL_HEADING']}")
                        print(f"    roll = {angles['EUL_ROLL']}")
                        print(f"   pitch = {angles['EUL_PITCH']}")

                    elif read_char in ['v', 'V']:
                        # Print our angular velocities
                        print("Print Angular Velocities (in degrees/second):")
                        velocities = angular_velocities.read()
                        print(f" X Acc = {velocities['GYR_DATA_X']}")
                        print(f" Y Acc = {velocities['GYR_DATA_Y']}")
                        print(f" Z Acc = {velocities['GYR_DATA_Z']}")

                    elif read_char in ['c', 'C']:
                        # Print our angular velocities
                        print(f"Print Control Constants:")
                        print(f"   Roll Motor Control (inner):")
                        print(f"      Kp = {prop_gain_roll.read()}")
                        print(f"      Kd = {deriv_gain_roll.read()}")
                        print(f"      Ki = {int_gain_roll.read()}")
                        print(f"   Pitch Motor Control (inner):")
                        print(f"      Kp = {prop_gain_pitch.read()}")
                        print(f"      Kd = {deriv_gain_pitch.read()}")
                        print(f"      Ki = {int_gain_pitch.read()}")
                        print(f"   Angle Control x (outer):")
                        print(f"      Kp = {positionx_prop_gain.read()}")
                        print(f"      Kd = {positionx_deriv_gain.read()}")
                        print(f"      Ki = {positionx_int_gain.read()}")
                        print(f"   Angle Control y (outer):")
                        print(f"      Kp = {positiony_prop_gain.read()}")
                        print(f"      Kd = {positiony_deriv_gain.read()}")
                        print(f"      Ki = {positiony_int_gain.read()}")

                    elif read_char in ['i', 'I']:
                        # Print our angular velocities
                        print(f"Print Angle Controller Info:")
                        print(f"    Target Roll Angle: {roll_target_pos.read()}")
                        print(f"   Target Pitch Angle: {pitch_target_pos.read()}")
                        print(f"   X Controller:")
                        print(f"      Kp: {controller_signalsx.read()['prop_val']}")
                        print(f"      Kd: {controller_signalsx.read()['deriv_val']}")
                        print(f"      Ki: {controller_signalsx.read()['int_val']}")
                        print(f"   Y Controller:")
                        print(f"      Kp: {controller_signalsy.read()['prop_val']}")
                        print(f"      Kd: {controller_signalsy.read()['deriv_val']}")
                        print(f"      Ki: {controller_signalsy.read()['int_val']}")


                    elif read_char in ['s', 'S']:
                        # End data collection prematurely
                        data_collect_flag.write(False)

                    elif read_char in ['d', 'D']:
                        # Print out devtools
                        printDevTools(print_queue)

                    elif read_char in ['x', 'X']:
                        # Kill the motors
                        roll_target_duty.write(0)
                        pitch_target_duty.write(0)
                        print("Motors Off.")

                    elif read_char in ['t', 'T']:
                        # Print current set points
                        print(f"x_pos: {x_target_position.read()} y_pos: {y_target_position.read()} roll: {roll_target_pos.read()} pitch: {pitch_target_pos.read()}")

                    elif read_char == 'm':
                        # Switch state to input duty cycle
                        # Also set inputTar (m1Flag) True to know where to write (motor 1)
                        print("Enter a duty cycle for motor 1:")
                        inputTar = roll_target_duty
                        buff = ""
                        state = S4_INPUT
                        return_state = S1_CMD

                    elif read_char == 'M':
                        # Switch state to input duty cycle
                        # Also set inputTar (m2Flag) True to know where to write (motor 2)
                        print("Enter a duty cycle for motor 2:")
                        inputTar = pitch_target_duty
                        buff = ""
                        state = S4_INPUT
                        return_state = S1_CMD

                    elif read_char in ['y']:
                        # Choose a Pitch set point for closed-loop control
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = pitch_target_pos
                        print("Enter a set point for pitch position:")

                    elif read_char in ['Y']:
                        # Choose a roll set point for closed-loop control
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = roll_target_pos
                        print("Enter a set point for roll position:")

                    elif read_char in ['k']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = prop_gain_roll
                        print("Enter a proportional gain (Kp) for the roll motor controller:")

                    elif read_char in ['j']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = deriv_gain_roll
                        print("Enter a derivative gain (Kd) for the roll motor controller:")

                    elif read_char in ['h']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = int_gain_roll
                        print("Enter a integral gain (Ki) for the roll motor controller:")

                    elif read_char in ['K']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = prop_gain_pitch
                        print("Enter a proportional gain (Kp) for the pitch motor controller:")

                    elif read_char in ['J']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = deriv_gain_pitch
                        print("Enter a derivative gain (Kd) for the pitch motor controller:")

                    elif read_char in ['H']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = int_gain_pitch
                        print("Enter a integral gain (Ki) for the pitch motor controller:")

                    elif read_char in ['1']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = positionx_prop_gain
                        print(
                            "Enter a proportional gain (Kp) for the angle x controller:")

                    elif read_char in ['2']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = positionx_deriv_gain
                        print(
                            "Enter a derivative gain (Kd) for the angle x controller:")

                    elif read_char in ['3']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = positionx_int_gain
                        print(
                            "Enter a integral gain (Ki) for the angle x controller:")

                    elif read_char in ['4']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = positiony_prop_gain
                        print(
                            "Enter a proportional gain (Kp) for the angle y controller:")

                    elif read_char in ['5']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = positiony_deriv_gain
                        print(
                            "Enter a derivative gain (Kd) for the angle y controller:")

                    elif read_char in ['6']:
                        # Set a new Kp for the angle controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = positiony_int_gain
                        print(
                            "Enter a integral gain (Ki) for the angle y controller:")

                    # elif read_char in ['K']:
                    #     # Set a new Kd for the controller
                    #     state = S4_INPUT
                    #     return_state = S1_CMD
                    #     buff = ""
                    #     inputTar = deriv_gain
                    #     print("Enter a derivative gain (Kd) for the platform controller:")

                    # elif read_char in ['u']:
                    #     # Set a new Kd for the controller
                    #     state = S4_INPUT
                    #     return_state = S1_CMD
                    #     buff = ""
                    #     inputTar = int_gain
                    #     print("Enter an integral gain (Ki) for the platform controller:")

                    elif read_char in ['j']:
                        # Set a new Kd for the controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = positionx_prop_gain
                        print("Enter a proportional gain (Kp) for the position controller:")

                    elif read_char in ['a']:
                        # Set a new target roll angle for controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = roll_target_pos
                        print(
                            "Enter a target roll angle for the controller:")

                    elif read_char in ['A']:
                        # Set a new target roll angle for controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = pitch_target_pos
                        print(
                            "Enter a target pitch angle for the controller:")

                    elif read_char in ['w', 'W']:
                        # Toggle whether the controller is enabled or disabled
                        cont_enable.write(not (cont_enable.read()))
                        print(f"Controller Enabled = {cont_enable.read()}")

                    elif read_char in ['r', 'R']:
                        # Go into the step response state
                        if not data_collect_flag.read():
                            print("Starting data collection.")
                            data_collect_flag.write(True)

                    elif read_char in ['l', 'L']:
                        # Print out x y position of touch panel
                        print_queue.quick_print(f"X: {(x_position_signal.read()):.3f}, Y: {(y_position_signal.read()):.3f}")

                    else:
                        print(f"Invalid character '{read_char}' entered\n")
                        # Clear the input buffer
                        serial_port.read()
                        printLegend(print_queue)

            elif state == S2_IMUCAL:
                # Wait for imu calibration status to be complete
                
                # Need to still be printing from queue while in this state
                if (print_queue.num_in() > 0):
                    print(print_queue.get_job())

                # Once calibration complete we can return
                if imu_calib_status.read():
                    print_queue.quick_print("IMU Calibration Complete!\n")
                    state = S0_INIT

            elif state == S3_PRINT:
                # Iterate through printing until reached the end
                # For this state, print a list of lists, each list will be printed in its own column

                if print_queue.num_in() == 0:
                    state = S1_CMD
                else:
                    print(print_queue.get_job())

            elif state == S4_INPUT:
                # This is where we read in multiple values to get a float value. buff is defined as
                # None when the state is changed to S4_INPUT. That way, whenever the input state is
                # called, buff is empty. I don't think there's an instance where the input state could
                # be called twice in a row without exiting the input state? --Caleb

                # Check if there is a character, and read it as a string.
                if serial_port.any():
                    char = serial_port.read(1).decode()

                    if char.isdigit():         # Check if it's a digit and add it.
                        buff += char
                        serial_port.write(char)
                    # Check if it's a minus at the beginning and add it.
                    elif char == '-' and buff == "":
                        buff += char
                        serial_port.write(char)
                    # Check if it's a backspace and NOT the first character (better method?)
                    elif char in ['\b', '\0x08', '\x7F'] and not buff == "":
                        buff = buff[:-1]
                        serial_port.write('\b')
                    # elif char in ['s', 'S'] and return_state == S5_TEST:
                    #     # Whenever S is pressed while in testing, need to quit immediately
                    #     s5_end_sig = True
                    #     state = S5_TEST
                    elif char in ['.'] and ('.' not in buff):
                        buff += '.'
                        serial_port.write(char)

                    # Check if it's enter, convert to float and assign
                    elif char in ['\r', '\n']:
                        if (buff == ""):                    # to target .share, then return to return_state
                            # This is a form of protection so that if they press enter on an empty string it won't accept it
                            pass
                        else:
                            val = float(buff)
                            serial_port.write(char)
                            print(f"Stored: {val}")
                            inputTar.write(val)
                            state = return_state
                            return_state = None
                            # inputTar.write(False)
                            new_duty = True
                            # Clear the buffer so its ready for next time
                            buff = ""

                    # Print the buffer after each character input
                    # print(buff)

            elif state == S5_PANELCAL:
                # Wait for touch panel calibration status to be complete

                # Need to still be printing from queue while in this state
                if (print_queue.num_in() > 0):
                    print(print_queue.get_job())

                # Once calibration is complete we can return to S0
                if touch_calib_status.read():
                    print_queue.quick_print("Touch Panel Calibration Complete!\n")
                    state = S0_INIT

            elif state == S6_SRESP:
                # First prompt user for proportional gain
                # Then, prompt user for derivative gain
                # Run step response for 3 sec window or until user interrupts with s key

                # Read in any chars from the keyboard
                if serial_port.any():
                    read_char = serial_port.read(1).decode()

                    if read_char in ['s', 'S']:
                        # End data collection prematurely
                        end_step_premature = True

                if not setGain:
                    # Disable control, motors, and set target positions to 0, 0
                    cont_enable.write(False)
                    roll_target_duty.write(0)
                    pitch_target_duty.write(0)
                    roll_target_pos.write(0)
                    pitch_target_pos.write(0)

                    state = S4_INPUT
                    print("Please enter a proportional gain Kp for the controller:")
                    inputTar = prop_gain
                    return_state = S6_SRESP
                    setGain = True
                    buff = ""

                elif not setDGain:
                    state = S4_INPUT
                    print("Please enter a derivative gain Kd for the controller:")
                    inputTar = deriv_gain
                    return_state = S6_SRESP
                    setDGain = True
                    buff = ""
                    # don't think this is needed as no driver is in use
                    # # need to enable controller
                    # cFlag.write(True)

                elif setGain and setDGain:
                    # We are now starting the step response
                    # print("Starting Step Response")

                    # When step_start_time is None we are just starting the response
                    if step_start_time is None:
                        print("Starting Step Response")
                        step_start_time = current_time
                        num_data_points = math.floor(
                            (3 * 10 ** 6) / step_sample_period)
                        test_roll_data = array.array('d', num_data_points*[0])
                        test_pitch_data = array.array('d', num_data_points*[0])
                        sample_number = 0
                        step_sample_next = step_start_time + step_sample_period

                    else:
                        try:
                            if ticks_diff(current_time, step_sample_next) >= 0:
                                # time to collect a new data point

                                test_roll_data[sample_number] = euler_angles['EUL_ROLL']
                                test_pitch_data[sample_number] = euler_angles['EUL_PITCH']
                                sample_number += 1
                                step_sample_next += step_sample_period

                        except MemoryError:
                            raise MemoryError(
                                "Error in filling up test data array")

                        # When less than 1 second, set duty cycle to 0
                        if (ticks_diff(current_time, step_start_time) < (10 ** 6)) and not end_step_premature:
                            # Should be disabled
                            cont_enable.write(False)
                        elif (ticks_diff(current_time, step_start_time) < (3 * 10 ** 6)) and not end_step_premature:
                            # Enable the controller
                            cont_enable.write(True)
                        else:
                            # We have exceeded the 3 seconds. Lets stop the motor
                            cont_enable.write(False)
                            roll_target_duty.write(0)
                            pitch_target_duty.write(0)
                            print("Step Response Finished")

                            # Write our data to the print queue
                            new_job = PrintJob("X Position, Y Position")
                            print_queue.add_job(new_job)

                            for i in range(len(test_roll_data)):
                                print_queue.add_job(
                                    PrintJob(f"{test_roll_data} {test_pitch_data}"))

                            # We're done collecting so lets finish up the State and return to S1_CMD

                            state = S1_CMD

                            setGain = False
                            setDGain = False

                            step_start_time = None
                            end_step_premature = False

                            sample_number = 0
                            test_roll_data = None
                            test_pitch_data = None

            else:
                raise ValueError(
                    f"Invalid state value in {task_name}: State {state} does not exist")

            # print(f"Task User state = {state}")
            yield state

        else:
            # Ticks diffnot yet ready to change state
            yield None
