'''!@file               main.py
    @brief              Main function to cycle through tasks and run them in a cooperative manner.
    @details            Executes tasks sequentially in a blocking manner. Tasks must avoid blocking themselves.

    @author             Caleb Savard
    @author             Chris Linthacum
    @date               March 18, 2022
'''
from taskPanel import taskPanelFcn
import shares
from taskUser import taskUserFcn
import taskMotor
import taskMotorController
import taskAngleController
import taskIMU
import time
import taskDataCollect

from printqueue import PrintQueue, PrintJob

def final_main():
    '''!@brief      Main driver executing various tasks
        @details    The main driver function for the project. Cycles through 6 task management functions representing
                    different tasks needed to run the system. For additional information, view the task and state diagrams
                    on the main project page. <br>
                    Tasks include:<br>
                    - taskPanelFcn <br>
                    - taskUserFcn <br>
                    - taskMotorFcn <br>
                    - taskMotorController <br>
                    - taskAngleController <br>
                    - taskIMU <br>
                    - taskDataCollect <br>
        @returns    None
    '''
    
    # Functional small ball constants:
    #   Inner Loop Kp = 4.3,    Kd = -0.2
    #   Outer Loop Kp = -0.09,  Kd = 0.03
    
    # Functional small ball constants (seperated)
    #   Inner Loop Kp = 4.3,    Kd = -0.2
    #   Outer Loop X: Kp = -0.09 Kd = 0.03
    #   Outer Loop Y: Kp = -0.11 Kd = 0.04
    zFlag = shares.Share(False)
    pFlag = shares.Share(False)
    dFlag = shares.Share(False)
    vFlag = shares.Share(False)
    gFlag = shares.Share(False)
    sFlag = shares.Share(False)
    roll_target_duty = shares.Share(None)
    pitch_target_duty = shares.Share(None)
    dataPrint = shares.Share(None)
    cFlag = shares.Share(False)
    tFlag = shares.Share(False)
    testData = shares.Share(False)
    input_signal = shares.Share(None)
    target_val = shares.Share(0)
    
    cont_enable = shares.Share(None)
    # target_duty = shares.Share(None)
    euler_angles = shares.Share(None)
    angular_velocities = shares.Share(None)
    imu_calib_status = shares.Share(None)
    
    roll_target_pos = shares.Share(0)
    pitch_target_pos = shares.Share(0)
    roll_target_deriv = shares.Share(0)
    pitch_target_deriv = shares.Share(0)
    x_target_position = shares.Share(0)
    y_target_position = shares.Share(0)
    x_position_signal = shares.Share(0)
    y_position_signal = shares.Share(0)
    ball_detected = shares.Share(False)
    panelCalStatus = shares.Share(False)
    touch_calib_status = shares.Share(False)
    
    x_pos_deriv_signal = shares.Share(0)
    x_pos_deriv_target = shares.Share(0)
    y_pos_deriv_signal = shares.Share(0)
    y_pos_deriv_target = shares.Share(0)

    prop_gain_roll = shares.Share(4.3)
    deriv_gain_roll = shares.Share(-0.2)
    int_gain_roll = shares.Share(0)

    prop_gain_pitch = shares.Share(4.3)
    deriv_gain_pitch = shares.Share(-0.2)
    int_gain_pitch = shares.Share(0)

    positionx_prop_gain = shares.Share(-0.09)
    positionx_deriv_gain = shares.Share(0.035)
    positionx_int_gain = shares.Share(0.0)

    positiony_prop_gain = shares.Share(-0.15)
    positiony_deriv_gain = shares.Share(0.06)
    positiony_int_gain = shares.Share(0)

    controller_signalsx = shares.Share({'prop_val': 0, 'deriv_val': 0, 'int_val': 0})
    controller_signalsy = shares.Share({'prop_val': 0, 'deriv_val': 0, 'int_val': 0})

    data_collect_flag = shares.Share(False)

    print_queue = PrintQueue()
    
    sample_period = 100000

    time.sleep(1)
    
    task_list = [taskUserFcn("Task User", 
                                20_000, 
                                roll_target_duty, 
                                pitch_target_duty, 
                                gFlag, 
                                sFlag, 
                                prop_gain_roll, 
                                deriv_gain_roll, 
                                cont_enable, 
                                euler_angles, 
                                angular_velocities, 
                                roll_target_pos, 
                                pitch_target_pos, 
                                x_target_position, 
                                y_target_position, 
                                print_queue,
                                ball_detected, 
                                x_position_signal, 
                                y_position_signal, 
                                imu_calib_status, 
                                touch_calib_status, 
                                positionx_prop_gain, 
                                int_gain_roll,
                                positionx_deriv_gain,
                                positionx_int_gain,
                                positiony_prop_gain,
                                positiony_deriv_gain,
                                positiony_int_gain,
                                controller_signalsx,
                                controller_signalsy,
                                prop_gain_pitch,
                                deriv_gain_pitch,
                                int_gain_pitch,
                                data_collect_flag),
                 taskMotor.taskMotorFcn("Task Motor", 
                                        10_000, 
                                        roll_target_duty, 
                                        pitch_target_duty),
                 taskMotorController.taskMotorControllerFcn("Task Motor Controller", 
                                                            10_000, 
                                                            euler_angles, 
                                                            angular_velocities, 
                                                            roll_target_pos, 
                                                            roll_target_deriv, 
                                                            pitch_target_pos, 
                                                            pitch_target_deriv, 
                                                            prop_gain_roll, 
                                                            deriv_gain_roll, 
                                                            int_gain_roll, 
                                                            cont_enable, 
                                                            roll_target_duty, 
                                                            pitch_target_duty,
                                                            prop_gain_pitch,
                                                            deriv_gain_pitch,
                                                            int_gain_pitch),
                 taskAngleController.taskAngleControllerFcn("Task Angle Controller", 
                                                            10_000, 
                                                            roll_target_pos, 
                                                            pitch_target_pos,
                                                            x_position_signal,
                                                            x_pos_deriv_signal,
                                                            x_target_position,
                                                            x_pos_deriv_target,
                                                            y_position_signal,
                                                            y_pos_deriv_signal,
                                                            y_target_position,
                                                            y_pos_deriv_target,
                                                            positionx_prop_gain,
                                                            positionx_deriv_gain,
                                                            positionx_int_gain,
                                                            cont_enable,
                                                            ball_detected,
                                                            print_queue,
                                                            positiony_prop_gain,
                                                            positiony_deriv_gain,
                                                            positiony_int_gain,
                                                            controller_signalsx,
                                                            controller_signalsy),
                 taskIMU.taskIMUFcn("Task IMU", 
                                    10_000, 
                                    euler_angles, 
                                    angular_velocities, 
                                    imu_calib_status, 
                                    print_queue),
                 taskPanelFcn("Task Panel", 
                                10_000, 
                                x_position_signal, 
                                y_position_signal, 
                                ball_detected,
                                panelCalStatus, 
                                print_queue, 
                                touch_calib_status, 
                                imu_calib_status, 
                                x_pos_deriv_signal, 
                                y_pos_deriv_signal),
                taskDataCollect.taskDataFcn("Task Data",
                                            100_000,
                                            data_collect_flag,
                                            x_position_signal,
                                            y_position_signal,
                                            ball_detected,
                                            euler_angles,
                                            print_queue)]

    while True:
        try:
            for task in task_list:
                next(task)
        except KeyboardInterrupt:
            break
    
    print("Terminating Program")



if __name__ == '__main__':
    final_main()

    # Tested gains were Kp = 4.5 Kd = -0.2
